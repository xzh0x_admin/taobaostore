# taobaostore(涛宝商城)
这个项目名字起源于我的一个同学~

#### 介绍
本商城主要实现了用户模块,订单模块,购物车模块,商品模块
这个商城也是作者别离校园生活的最后一个项目~~

#### 页面展示:

 **网站演示地址** :

前台商城主页:[http://taobaostore.itsnkkka.cn](http://taobaostore.itsnkkka.cn)

后台管理系统:[http://taobaostore.itsnkkka.cn/admin](http://taobaostore.itsnkkka.cn/admin)

 **后台管理系统测试账户** :

账号:xuezipeng

密码:123123

#### 1.涛宝商城前台主页
![涛宝商城前台主页](https://images.gitee.com/uploads/images/2020/0120/155205_05c6a7f6_5197209.png "taobaostore-index.png")
#### 2.涛宝商城前台登录
![涛宝商城前台登录](https://images.gitee.com/uploads/images/2020/0120/155220_a9d0e360_5197209.png "taobaostore-login.png")
#### 3.涛宝商城前台购物车
![涛宝商城前台购物车](https://images.gitee.com/uploads/images/2020/0120/155256_c4ce265c_5197209.png "taobaostore-cart.png")
#### 4.涛宝商城前台订单
![涛宝商城前台订单](https://images.gitee.com/uploads/images/2020/0120/155323_75f7a3ff_5197209.png "taobaostore-order.png")
#### 5.涛宝商城前台支付(支付宝沙箱环境)
![涛宝商城前台支付](https://images.gitee.com/uploads/images/2020/0120/155346_3ef44a72_5197209.png "taobaostore-order-pay.png")
#### 6.涛宝商城前台用户系统
![涛宝商城前台用户系统](https://images.gitee.com/uploads/images/2020/0120/155405_20245425_5197209.png "taobaostore-user.png")
#### 7.涛宝商城前台团队介绍
![涛宝商城前台团队介绍](https://images.gitee.com/uploads/images/2020/0120/155421_d3818fb2_5197209.png "taobaostore-team.png")
#### 8.涛宝商城前台订单地址
![涛宝商城前台订单地址](https://images.gitee.com/uploads/images/2020/0120/155441_6d461b21_5197209.png "taobaostore-
address.png")
#### 9.涛宝商城后台登录页面
![涛宝商城后台登录页面](https://images.gitee.com/uploads/images/2020/0120/155504_a7b95c1c_5197209.png "taobaostore-admin-login.png")
#### 10.涛宝商城后台主页
![涛宝商城后台主页](https://images.gitee.com/uploads/images/2020/0120/155523_fec1a21d_5197209.png "taobaostore-admin-index.png")
#### 11.涛宝商城后台分类管理
![涛宝商城后台分类管理](https://images.gitee.com/uploads/images/2020/0120/155535_57dc888c_5197209.png "taobaostore-admin-group.png")
#### 12.涛宝商城后台商品管理
![涛宝商城后台商品管理](https://images.gitee.com/uploads/images/2020/0120/155552_c23330af_5197209.png "taobaostore-admin-product.png")
#### 13.涛宝商城后台订单管理
![涛宝商城后台订单管理](https://images.gitee.com/uploads/images/2020/0120/155619_fbb3fae2_5197209.png "taobaostore-admin-order.png")
#### 软件架构
软件架构说明

核心框架:Spring5.0.2.RELEASE,SpringMVC5.0.2.RELEASE,Mybatis3.4.5

数据库连接:Alibaba Druid

前后端交互:Ajax,Thymeleaf(模板引擎)

json序列化:jackson

缓存:Redis

关系型数据库:Mysql

前端页面技术:html+css+javascript+jquery,[MDUI](https://www.mdui.org/),[Bootstrap](https://www.bootcss.com/)

#### 安装教程

1.  修改datasource.properties文件中数据库连接属性
2.  修改ftpDataSource.properties文件中的FTP图片服务器信息(本项目的图片一律通过FTP协议存放在远程服务器中),如果没有远程服务器可以用作者的(信息已经填写在项目中,不改就行了!)

```
FTP_IP=49.232.168.42
FTP_PORT=21
FTP_USERNAME=taobaoimg
FTP_PASSWORD=123456
```

3.  修改ftpDataSource.properties文件中的alipay.callback.url,这个是支付宝的订单回调通知地址,必须保证是外网可以访问到的地址,如果没有云服务器这里建议本地通过内网穿透来开放外网可以访问的地址,推荐[花生壳](https://hsk.oray.com/),Natapp,Ngrok等内网穿透工具

4.由于我们使用的是支付宝沙箱环境，可能会发生jar包丢失，因为沙箱的jar没有在maven仓库中
因此大家可能需要手动导入一下：
https://oss.itsnkkka.cn/taobaostore/alipay-trade-sdk-20161215.jar
#### 使用说明

- com.alipay.demo.trade
- com.taobaostore
  - common  #存放各种通用类
  - dao     #与数据库交互
  - pojo    #实体类
  - service #业务逻辑
  - util    #存放各种工具类
  - vo      #value Object(展示给前端的实体类)
  - web.controller #控制层

#### 贡献者

 **薛梓芃 联系方式(QQ)2531639937** 

 **邱实   联系方式(QQ)1938510250** 

 **程卓   联系方式(QQ)2334824478** 

 **杨涛   联系方式(QQ)1254896553** 

 **赵帅   联系方式(QQ)1421369107** 

 **韩金航 联系方式(QQ)2569536258** 

 **本项目会持续更新,后期将改造为SpringBoot,SpringCloud作为核心框架的项目!!!** 
