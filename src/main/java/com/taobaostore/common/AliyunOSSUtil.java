package com.taobaostore.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.UUID;

import org.springframework.web.multipart.MultipartFile;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.model.PutObjectResult;

public class AliyunOSSUtil {

	private static Properties properties = null;
	// 阿里云OSS对象存储地区 例如:https://oss-cn-beijing.aliyuncs.com
	private static String endpoint = null;
	// 主账号AccessKey拥有所有API的访问权限，风险很高。强烈建议您创建并使用RAM账号进行API访问或日常运维，请登录RAM控制台创建RAM账号。
	private static String accessKeyId = null;
	private static String accessKeySecret = null;
	// bucket名称
	private static String bucketName = null;
	// 文件夹名
	public static String objectNamePrefix = null;
	// 阿里云OSS绑定的域名
	public static String aliyunOSSURL = null;
	// 完整URL = 域名+文件夹名
	public static String fullURL = null;
	static {
		try {
			properties = new Properties();
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("aliyun-oss.properties"));
			endpoint = properties.getProperty("endpoint");
			accessKeyId = properties.getProperty("accessKeyId");
			accessKeySecret = properties.getProperty("accessKeySecret");
			bucketName = properties.getProperty("bucketName");
			objectNamePrefix = properties.getProperty("objectNamePrefix");
			aliyunOSSURL = properties.getProperty("aliyunossURL");
			fullURL = aliyunOSSURL + "/" + objectNamePrefix + "/";
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 直接上传文件，返回是否上传成功
	 * @param fileName
	 * @param fileIn
	 * @return
	 */
	public static boolean fileUpload(String fileName,InputStream fileIn) {
		try {
			String objectName = objectNamePrefix+"/"+fileName;

			// 创建OSSClient实例。
			OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

			// 上传文件到指定的存储空间（bucketName）并将其保存为指定的文件名称（objectName）。
			PutObjectResult PutObjectResult = ossClient.putObject(bucketName, objectName, fileIn);
	        
			// 关闭OSSClient。
			ossClient.shutdown(); 
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
	
	/**
	 * 上传文件 上传成功后直接返回文件名
	 * @param file
	 * @return
	 */
	public static String fileUpload(MultipartFile file) {
		String fileName = null;
		try {
			String fileSuffix = file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
			fileName = UUID.randomUUID().toString().replace("==", "") + fileSuffix;
			String objectName = objectNamePrefix+"/"+fileName;

			// 创建OSSClient实例。
			OSS ossClient = new OSSClientBuilder().build(endpoint, accessKeyId, accessKeySecret);

			// 上传文件到指定的存储空间（bucketName）并将其保存为指定的文件名称（objectName）。
			ossClient.putObject(bucketName, objectName, file.getInputStream());
	        
			// 关闭OSSClient。
			ossClient.shutdown(); 
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return fullURL + fileName;
	}
	
	
}
