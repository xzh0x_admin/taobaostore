package com.taobaostore.common;

import javax.servlet.ServletContext;

import org.springframework.beans.factory.annotation.Autowired;

public class Const {
	
	public static final String CURRENT_USER = "currentUser";
	public static final String USER_NAME = "username";
	public static final String EMAIL = "email";
	public static final String USER_PIC = System.getProperty("catalina.home")+"/webapps/taobaouser_head/";// 用户头像存储路径
	public static final String PRODUCT_ID = "product_id";
	public static final String PRODUCT_MSG = "product_msg";
	public static final String KEY_WORD = "keyword";
	public static final String FRONT_PRODUCT_MSG = "front_product_msg";
	
	// 使用内部接口实现权限组分离
	public interface Role{
		int ROLE_USER = 1;// 普通用户
		int ROLE_ADMIN = 0;// 管理员用户
	}
	
	public interface Cart{
		int CHECKED = 1;//购物车选中状态
		int ON_CHECKED = 0;// 购物车未选中
		
		String LIMIT_NUM_FAIL = "LIMIT_NUM_FAIL";
		String LIMIT_NUM_SUCCESS = "LIMIT_NUM_SUCCESS";
	}
	
	// 支付状态常量
	public enum OrderStatusEnum {
		CANCELED(0,"已取消"),
		NO_PAY(10,"未支付"),
		PAID(20,"已付款"),
		SHIPPED(40,"已发货"),
		ORDER_SUCCESS(50,"订单完成"),
		ORDER_CLOSE(60,"订单关闭");
		
		private String value;
		private int code;
		
		OrderStatusEnum(int code, String value) {
			this.code = code;
			this.value = value;
		}
		
		public int getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
	}
	
	// 支付宝回调常量
	public interface AlipayCallback{
		String TRADE_STATUS_WAIT_BUYER_PAY = "WAIT_BUYER_PAY";//交易创建
		String TRADE_STATUS_TRADE_SUCCESS = "TRADE_SUCCESS";//交易成功
		
		String RESPONSE_SUCCESS="success";
		String RESPONSE_FAILED="faled";
	}
	
	// 支付类型常量
	public enum PayPlatformEnum{
		ALIPAY(1,"支付宝");
		
		private String value;
		private int code;		
		
		PayPlatformEnum(int code, String value) {
			this.code = code;
			this.value = value;
		}

		public int getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
	}
	
	public enum PaymentType{
		ON_LINE(1,"在线支付");
		
		private String value;
		private int code;		
		
		PaymentType(int code, String value) {
			this.code = code;
			this.value = value;
		}

		public int getCode() {
			return code;
		}
		
		public String getValue() {
			return value;
		}
	}
	
	    // 商品状态常量   1-在售,2-下架,3-删除
		public enum ProductStatus{
			SALL(1,"在售"),
			SOLDOUT(2,"下架"),
			DELETE(3,"删除");
			
			private String value;
			private int code;		
			
			ProductStatus(int code, String value) {
				this.code = code;
				this.value = value;
			}

			public int getCode() {
				return code;
			}
			
			public String getValue() {
				return value;
			}
		}
	
}
