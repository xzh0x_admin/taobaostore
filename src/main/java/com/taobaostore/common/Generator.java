package com.taobaostore.common;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.plexus.personality.plexus.lifecycle.phase.Configurable;
import org.mybatis.generator.api.MyBatisGenerator;
import org.mybatis.generator.config.Configuration;
import org.mybatis.generator.config.xml.ConfigurationParser;
import org.mybatis.generator.exception.XMLParserException;
import org.mybatis.generator.internal.DefaultShellCallback;

public class Generator {
	public static void main(String[] args) throws Exception{
        //MBG执行过程中的警告信息:
		List<String> warnings= new ArrayList<String>();
		//生成代码重复时,是否覆盖源代码
		boolean override=false;
		InputStream in=Thread.currentThread().getContextClassLoader().getResourceAsStream("generatorConfig.xml");
	    ConfigurationParser cp=new ConfigurationParser(warnings);
	    Configuration config=cp.parseConfiguration(in);
	    DefaultShellCallback callback=new DefaultShellCallback(override);
	    
	    //创建MGB
	    MyBatisGenerator mbg=new MyBatisGenerator(config, callback, warnings);
	    mbg.generate(null);
	    for (String string : warnings) {
			System.out.println(string);
		}
	}
}