package com.taobaostore.common;


public enum ResponseCode {
	SUCCESS(0,"SUCCESS"),// 响应成功
    ERROR(1,"ERROR"),    // 响应失败
    NEED_LOGIN(10,"NEED_LOGIN"), // 登录错误
	ILLEGAL_ARGUMENT(2,"ILLEGAL_ARGUMENT"); // 非法参数
	
    private final int CODE;
	private final String DESC;
	
    ResponseCode(int code,String desc){
    	this.CODE = code;
    	this.DESC = desc;
    }

	public int getCODE() {
		return CODE;
	}

	public String getDESC() {
		return DESC;
	}
}
