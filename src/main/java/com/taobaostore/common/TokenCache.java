package com.taobaostore.common;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;

public class TokenCache {
	public static final String TOKEN_NAME = "token_";
	// com.google.common.cache
	private static LoadingCache<String,String> localCache = CacheBuilder.newBuilder().initialCapacity(1000).maximumSize(1000)
			.build(new CacheLoader<String, String>(){
				/**
				 * 默认的数据加载实现，如果key没有对应的值,就调用这个方法进行加载
				 */
				public String load(String key) throws Exception {
					return "null";
				}; 
			});
	public static void setKey(String key,String value) {
		localCache.put(key, value);
	}
	public static String getKey(String key) {
		String value = null;
		try {
			value = localCache.get(key);
			if ("null".equals(value)) {
				return null;
			}
			return value;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 调用
	 * TokenCache,setKey(token + username,'forgetToken随机数');
	 */
}
