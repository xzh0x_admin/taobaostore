package com.taobaostore.common;

import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.taobaostore.pojo.User;

public class UserContext {
	
	private static HttpSession getSession() {
		return ((ServletRequestAttributes)(RequestContextHolder.getRequestAttributes()))
				.getRequest().getSession();
	}

	public static User getUser() {
		User user = (User)getSession().getAttribute(Const.CURRENT_USER);
		return user;
	}
}