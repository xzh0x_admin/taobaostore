package com.taobaostore.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.taobaostore.pojo.Cart;

public interface CartMapper {
    int insert(Cart record);

    int insertSelective(Cart record);
    
    int updateByPrimaryKeySelective(Cart record);
    
    Cart selectCartByUserIdProductId(@Param("userId")Integer userId,@Param("productId")Integer productId);
    
    List<Cart> selectCartByUserId(Integer userId);
    
   int selectCartProductCheckedStatusByUserId(int userId);
   
   int deleteByUserIdProductIds(@Param("userId")Integer userId,@Param("productIds")List<String> productIds);
   
   int deleteByCartId(Integer cartId);
   
   int updateByCheckPitchAll(@Param("userId")Integer userId);
   
   int updateByCheckInvertAll(@Param("userId")Integer userId);
   
   int updateByCheckPitch(@Param("userId")Integer userId,@Param("productId")Integer productId);
   
   int updateByCheckInvert(@Param("userId")Integer userId,@Param("productId")Integer productId);

   List<Cart> listCartByUserIdAndChecked(Integer userId);
}