package com.taobaostore.dao;

import java.util.List;

import com.taobaostore.pojo.Category;

public interface CategoryMapper {
    int insert(Category record);

    int insertSelective(Category record);
    
    int updateByPrimaryKeySelective(Category category);
    
    List<Category> listCategory();// 查询所有商品种类
    
    List<Category> listCategoryChildren(int categoryId);// 查询指定商品种类下属的种类
    
    Category getCategoryById(int id);// 根据种类id查询
    
    List<Category> listCategoryByParentId(int parent_id);// 根据父种类获取指定种类列表
}