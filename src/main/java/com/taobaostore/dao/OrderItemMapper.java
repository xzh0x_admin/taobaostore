package com.taobaostore.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.taobaostore.pojo.OrderItem;

public interface OrderItemMapper {
    int insert(OrderItem record);

    int insertSelective(OrderItem record);
    
    List<OrderItem> listOrderItemByIdAndUserId(@Param("userId")Integer userId,@Param("orderOn")Long orderOn);
    
    List<OrderItem> listOrderItemByOrderOn(@Param("orderOn")Long orderOn);
    
    List<OrderItem> listOrderItemById(@Param("userId")Integer userId);
    
    int updateByPrimaryKeySelective(OrderItem record);
    
    int batchOrderItem(@Param("listOrderItem")List<OrderItem> listOrderItem);
}