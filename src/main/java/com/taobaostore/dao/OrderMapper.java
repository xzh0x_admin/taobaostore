package com.taobaostore.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.taobaostore.pojo.Order;

public interface OrderMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(Order record);

    int insertSelective(Order record);

    Order selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(Order record);

    int updateByPrimaryKey(Order record);
    
    Order selectByUserIdAndOrderNo(@Param("userId")Integer userId,@Param("orderNo")Long orderNo);
    
    Order selectByOrderNo(Long orderNo);
    
    int updateOrderStatus(@Param("userId")Integer userId,@Param("orderId")long orderId);
    
    List<Order> listOrderByUserId(@Param("userId")Integer userId);
    
    List<Order> listOrder();
    
    int countOrder();
}