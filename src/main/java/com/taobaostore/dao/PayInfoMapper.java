package com.taobaostore.dao;

import com.taobaostore.pojo.PayInfo;

public interface PayInfoMapper {
    int insert(PayInfo record);

    int insertSelective(PayInfo record);
    
    PayInfo getPayInfoByOrderOn(Long orderOn);
}