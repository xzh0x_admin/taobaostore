package com.taobaostore.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.taobaostore.pojo.Product;

public interface ProductMapper {
    int insert(Product record);

    int insertSelective(Product record);
    
    List<Product> listProduct();

    int updateByPrimaryKeySelective(Product record);
    
    int CountProduct();
    
    List<Product> listProductByName(String name);
    
    Product getProductById(int id);
    
    Product getProductIdStatus(int id);
    
    List<Product> selectList();

    List<Product> selectByNameAndProductId(@Param("productName")String productName,@Param("productId") Integer productId);

    List<Product> selectByNameAndCategoryIds(@Param("productName")String productName,@Param("categoryIdList")List<Integer> categoryIdList);
} 