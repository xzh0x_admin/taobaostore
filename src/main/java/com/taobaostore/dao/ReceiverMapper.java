package com.taobaostore.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.taobaostore.pojo.Receiver;

public interface ReceiverMapper {
    int insert(Receiver record);

    int insertSelective(Receiver record);
    
    /**
     * 查询指定用户下的所有收货地址信息
     * @param userId
     * @return
     */
    List<Receiver> listReceiverByUserId(int userId);
    
    
    int deleteReceiverById(@Param("userId")int userId,@Param("id")int id);
    
    Receiver getReceiverById(int receiverId);
}