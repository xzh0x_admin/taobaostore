package com.taobaostore.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.taobaostore.pojo.User;

public interface UserMapper {
    int insert(User record);

    int insertSelective(User record);
    
    List<User> listUser();
    
    int updateByPrimaryKeySelective(User user);
    
    int checkUserName(@Param("username")String username);
    
    int checkEmail(@Param("email")String email);
    
    User selectLogin(@Param("username")String username,@Param("password")String password);
    
    String selectQuestion(@Param("username")String username);
    
    int selectAnswer(@Param("username")String username,@Param("question")String question,@Param("answer")String answer);
    
    void updatePwd(@Param("username")String username,@Param("password")String password);
    
    int selectPassword(@Param("password")String password,@Param("id")int userid);
}