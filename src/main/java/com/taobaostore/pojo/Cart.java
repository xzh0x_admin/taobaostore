package com.taobaostore.pojo;

import java.util.Date;
// 购物车表
public class Cart {
	private Integer id;// 购物车id

	private Integer userId;// 所属用户id

	private Integer productId;// 购物商品id

	private Integer quantity;// 商品数量

	private Integer checked;// 是否选中(1-已经选择 0-没有选中)

	private Date createTime;// 创建时间

	private Date updateTime;// 更新时间


    public Cart(Integer id, Integer userId, Integer productId, Integer quantity, Integer checked, Date createTime,
			Date updateTime) {
		this.id = id;
		this.userId = userId;
		this.productId = productId;
		this.quantity = quantity;
		this.checked = checked;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	public Cart() {
        super();
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getChecked() {
        return checked;
    }

    public void setChecked(Integer checked) {
        this.checked = checked;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

	@Override
	public String toString() {
		return "Cart [id=" + id + ", userId=" + userId + ", productId=" + productId + ", quantity=" + quantity
				+ ", checked=" + checked + ", createTime=" + createTime + ", updateTime=" + updateTime + "]";
	}
    
}