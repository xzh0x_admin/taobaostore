package com.taobaostore.pojo;

import java.math.BigDecimal;
import java.util.Date;
// 订单信息表
public class Order {
	private Integer id;// 订单表id

	private Long orderNo;// 订单号

	private Integer userId;// 用户id

	private Integer receiverId;// 送货地址id

	private BigDecimal payment;// 实际付款金额,单位是元,保留两位小数

	private Integer paymentType;// 付款方式(在线支付)

	private Integer postage;// 运费(元)

	private Integer status;// 订单状态(订单状态:0-取消,10-未付款,20-已付款,40-已发货,50-交易成功,60-交易关闭)

	private Date paymentTime;// 支付时间

	private Date sendTime;// 发货时间

	private Date endTime;// 交易结束时间

	private Date closeTime;// 交易关闭时间

	private Date createTime;// 订单创建时间

	private Date updateTime;// 订单更新时间

    

    public Order(Integer id, Long orderNo, Integer userId, Integer receiverId, BigDecimal payment, Integer paymentType,
			Integer postage, Integer status, Date paymentTime, Date sendTime, Date endTime, Date closeTime,
			Date createTime, Date updateTime) {
		this.id = id;
		this.orderNo = orderNo;
		this.userId = userId;
		this.receiverId = receiverId;
		this.payment = payment;
		this.paymentType = paymentType;
		this.postage = postage;
		this.status = status;
		this.paymentTime = paymentTime;
		this.sendTime = sendTime;
		this.endTime = endTime;
		this.closeTime = closeTime;
		this.createTime = createTime;
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "Order [id=" + id + ", orderNo=" + orderNo + ", userId=" + userId + ", receiverId=" + receiverId
				+ ", payment=" + payment + ", paymentType=" + paymentType + ", postage=" + postage + ", status="
				+ status + ", paymentTime=" + paymentTime + ", sendTime=" + sendTime + ", endTime=" + endTime
				+ ", closeTime=" + closeTime + ", createTime=" + createTime + ", updateTime=" + updateTime + "]";
	}

	public Order() {
		
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Long getOrderNo() {
        return orderNo;
    }

    public void setOrderNo(Long orderNo) {
        this.orderNo = orderNo;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Integer receiverId) {
        this.receiverId = receiverId;
    }

    public BigDecimal getPayment() {
        return payment;
    }

    public void setPayment(BigDecimal payment) {
        this.payment = payment;
    }

    public Integer getPaymentType() {
        return paymentType;
    }

    public void setPaymentType(Integer paymentType) {
        this.paymentType = paymentType;
    }

    public Integer getPostage() {
        return postage;
    }

    public void setPostage(Integer postage) {
        this.postage = postage;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Date getPaymentTime() {
        return paymentTime;
    }

    public void setPaymentTime(Date paymentTime) {
        this.paymentTime = paymentTime;
    }

    public Date getSendTime() {
        return sendTime;
    }

    public void setSendTime(Date sendTime) {
        this.sendTime = sendTime;
    }

    public Date getEndTime() {
        return endTime;
    }

    public void setEndTime(Date endTime) {
        this.endTime = endTime;
    }

    public Date getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(Date closeTime) {
        this.closeTime = closeTime;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }
}