package com.taobaostore.pojo;

import java.math.BigDecimal;
import java.util.Date;
// 订单子表(订单详细)
public class OrderItem{
	private Integer id;// 订单详细表id

    private Long orderOn;// 订单表id(其实父表已经有了,以此增加查询速度)

    private Integer productId;// 商品id

    private String productName;// 商品名称

    private String productImage;// 商品图片

    private BigDecimal currentUnitPrice;// 当前单价

    private Integer quantity;// 商品数量

    private BigDecimal totalPrice;// 商品总价

    private Date createTime;// 创建时间

    private Date updateTime;// 更新时间

    private Integer userId;// 用户id
    
    public OrderItem() {
    	
    }
    
    public OrderItem(Integer id, Integer userId, Long orderOn, Integer productId, String productName, String productImage, BigDecimal currentUnitPrice, Integer quantity, BigDecimal totalPrice, Date createTime, Date updateTime) {
        this.id = id;
        this.userId = userId;
        this.orderOn = orderOn;
        this.productId = productId;
        this.productName = productName;
        this.productImage = productImage;
        this.currentUnitPrice = currentUnitPrice;
        this.quantity = quantity;
        this.totalPrice = totalPrice;
        this.createTime = createTime;
        this.updateTime = updateTime;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Long getOrderNo() {
        return orderOn;
    }

    public void setOrderNo(Long orderNo) {
        this.orderOn = orderNo;
    }

    public Integer getProductId() {
        return productId;
    }

    public void setProductId(Integer productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName == null ? null : productName.trim();
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage == null ? null : productImage.trim();
    }

    public BigDecimal getCurrentUnitPrice() {
        return currentUnitPrice;
    }

    public void setCurrentUnitPrice(BigDecimal currentUnitPrice) {
        this.currentUnitPrice = currentUnitPrice;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

	@Override
	public String toString() {
		return "OrderItem [id=" + id + ", orderOn=" + orderOn + ", productId=" + productId + ", productName="
				+ productName + ", productImage=" + productImage + ", currentUnitPrice=" + currentUnitPrice
				+ ", quantity=" + quantity + ", totalPrice=" + totalPrice + ", createTime=" + createTime
				+ ", updateTime=" + updateTime + ", userId=" + userId + "]";
	}
    
}