package com.taobaostore.service;

import com.taobaostore.common.ServerResponse;
import com.taobaostore.vo.CartVo;

public interface ICartService {
	/**
	 * 查询当前用户的所有购物车信息
	 * @param userId
	 * @return
	 */
	ServerResponse<CartVo> list(Integer userId);
	/**
	 * 新增购物车信息
	 * @param userId
	 * @param productId
	 * @param count
	 * @return
	 */
	ServerResponse<CartVo> add(Integer userId,Integer productId,Integer count);
	/**
	 * 更新购物车信息
	 * @param userId
	 * @param productId
	 * @param count
	 * @return
	 */
	ServerResponse<CartVo> update(Integer userId,Integer productId,Integer count);
	/**
	 * 删除购物车信息
	 * @param userId
	 * @param productIds
	 * @return
	 */
	ServerResponse<CartVo> deleteProduct(Integer userId,String productIds);
	/**
	 * 全选当前购物车中的商品
	 * @param userId
	 * @return
	 */
	ServerResponse<CartVo> checkedAll(Integer userId);
	/**
	 * 全反选当前购物车中的商品
	 * @param userId
	 * @return
	 */
	ServerResponse<CartVo> UncheckedAll(Integer userId);
	/**
	 * 选中某个商品
	 * @param userId
	 * @param productId
	 * @return
	 */
	ServerResponse<CartVo> checkPitch(Integer userId,Integer productId);
	/**
	 * 反选某个商品
	 * @param userId
	 * @param productId
	 * @return
	 */
	ServerResponse<CartVo> UncheckPitch(Integer userId,Integer productId);
}
