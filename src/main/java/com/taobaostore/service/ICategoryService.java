package com.taobaostore.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.taobaostore.common.ServerResponse;
import com.taobaostore.pojo.Category;

public interface ICategoryService {
	/**
	 * 获取指定类型品类下的所有品类信息
	 * @param parent_id
	 * @return
	 */
	ServerResponse<List<Category>> listCategoryByParentId(int parent_id);
	/**
	 * 返回所有节点
	 * @return
	 */
	ServerResponse<List<Category>> listCategory();
	/**
	 * 新增一个分类
	 * @param categoryName
	 * @param parentId
	 * @return
	 */
	ServerResponse addCategory(String categoryName,Integer parentId);
	
	/**
	 * 更新一个分类信息 根据分类id修改分类Name
	 * @param categoryId
	 * @param categoryName
	 * @return
	 */
	ServerResponse updateCategory(Integer categoryId,String categoryName);
	
	/**
	 * 查询指定节点下的子节点 不递归
	 * @param categoryId
	 * @return
	 */
	ServerResponse getChildrenParallelCategroy(Integer categoryId);
	
	/**
	 * 递归输出指定节点下的所有节点的id
	 * @param categoryId
	 */
	ServerResponse selectCategoryAndDeepChildrenByrenId(Integer categoryId);
}
