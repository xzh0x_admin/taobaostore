package com.taobaostore.service;

import org.springframework.web.multipart.MultipartFile;

import com.taobaostore.common.ServerResponse;

public interface IFileService {
	ServerResponse<String> upload(MultipartFile file,String path);
}
