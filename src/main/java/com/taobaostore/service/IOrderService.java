package com.taobaostore.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.PageInfo;
import com.taobaostore.common.ServerResponse;
import com.taobaostore.vo.CartProductVo;
import com.taobaostore.vo.CartVo;
import com.taobaostore.vo.OrderVo;

public interface IOrderService {
	// 生成支付宝二维码
	public ServerResponse<Map<String, String>> pay(Long orderNo,Integer userId,String path);
    // 支付宝回调函数
	ServerResponse aliCallback(Map<String,String> params);
	
	ServerResponse queryOrderPayStatus(Integer userId,Long orderNo);
	
	ServerResponse<Object> create(Integer userId,Integer receiverId);
	
	ServerResponse<CartVo> listCartProductVo(Integer userId);
	
	ServerResponse<List<OrderVo>> getOrderVo(Integer userId);
	
	ServerResponse<String> cancelOrder(Integer userId,Long orderOn);
	
	ServerResponse<PageInfo<OrderVo>> manageList(Integer pageNum,Integer pageSize);
	
	ServerResponse<String> updateOrderStatus(Integer userId,Long orderOn,Integer statusCode);
}
