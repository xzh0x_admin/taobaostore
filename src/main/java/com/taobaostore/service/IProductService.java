package com.taobaostore.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.taobaostore.common.ServerResponse;
import com.taobaostore.pojo.Product;
import com.taobaostore.vo.ProductDetailVo;
import com.taobaostore.vo.ProductListVo;

public interface IProductService {
	/**
	 * 查询全部商品信息
	 * @return
	 */
	List<Product> listProduct(); 
	/**
	 * 新增商品信息
	 * @param product
	 * @return
	 */
	ServerResponse<String> insertProduct(Product product);
	/**
	 * 新增或更新商品信息
	 * @param product
	 * @return
	 */
	ServerResponse saveOrUpdateProduct(Product product);

	/**
	 * 更新商品的状态(1-在售 2-下架 3-删除)
	 * @param productId
	 * @param status
	 * @return
	 */
	ServerResponse<String> setProductStatus(Integer productId,Integer status);
	
	/**
	 * 分页返回商品信息
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	ServerResponse<PageInfo<ProductListVo>> listProduct(Integer pageNum,Integer pageSize);
	
	/**
	 * 按照id查询出商品信息
	 * @param productId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	ServerResponse<PageInfo<ProductListVo>> getProductById(int productId,Integer pageNum,Integer pageSize);
	
	
	/**
	 * 根据姓名模糊查询
	 * @param productName
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	ServerResponse<PageInfo<ProductListVo>> listProductByName(String productName,Integer pageNum,Integer pageSize);
	
	/**
	 * 按照id查询出商品详细信息
	 * @param productId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	ServerResponse<PageInfo<ProductDetailVo>> getProductByIdDetail(int productId,Integer pageNum,Integer pageSize);

	/**
	 * 前台返回商品详细信息
	 * @param productId
	 * @return
	 */
	ServerResponse<ProductDetailVo> getProductDetail(Integer productId);
	
	/**
	 * 按照关键词或类型搜索
	 * @param keyword
	 * @param categoryId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	ServerResponse<PageInfo> getProductByKeywordCategory(String keyword,Integer categoryId,int pageNum,int pageSize);
}
