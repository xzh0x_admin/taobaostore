package com.taobaostore.service;

import com.github.pagehelper.PageInfo;
import com.taobaostore.common.ServerResponse;
import com.taobaostore.pojo.Receiver;

public interface IReceiverService {
	
	/**
	 * 新增地址
	 * @param receiver
	 * @return
	 */
	ServerResponse<String> add(Receiver receiver);
	
	
	/**
	 * 获取指定用户下的所有地址信息
	 * @param pageNum
	 * @param pageSize
	 * @param userId
	 * @return
	 */
	ServerResponse<PageInfo<Receiver>> list(int pageNum,int pageSize,Integer userId);
	
	
	/**
	 * 删除指定地址
	 * @param receiverId
	 * @return
	 */
	ServerResponse<String> delete(Integer userId,Integer receiverId);
}
