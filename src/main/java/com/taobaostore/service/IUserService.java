package com.taobaostore.service;

import org.springframework.web.multipart.MultipartFile;

import com.taobaostore.common.ServerResponse;
import com.taobaostore.pojo.User;

public interface IUserService {
	/**
	 * 登录系统
	 * @param username
	 * @param password
	 * @return
	 */
	ServerResponse<User> login(String username,String password);
	/**
	 * 验证用户信息
	 * @param type
	 * @param str
	 * @return
	 */
	ServerResponse<String> checkValid(String type,String str);
	/**
	 * 用户注册系统
	 * @param user
	 * @return
	 */
	ServerResponse<String> register(User user);
	
	/**
	 * 验证用户以及密保是否存在
	 * @param username
	 * @return
	 */
	ServerResponse<String> selectQuestion(String username);
	
	/**
	 * 查询指定用户的密保答案
	 * @param username
	 * @param question
	 * @param answer
	 * @return
	 */
	ServerResponse<String> checkQuestion(String username,String question,String answer);
	
	/**
	 * 执行找回密码操作
	 * @param username
	 * @param passwordNew
	 * @param forgetToken
	 * @return
	 */
	ServerResponse<String> forgetResetPassword(String username,String passwordNew,String forgetToken);
	
	/**
	 * 保存用户信息
	 * @param user
	 * @return
	 */
	ServerResponse<User> updateUserMsg(User user,MultipartFile pic);
	
	/**
	 * 在用户存在的情况下保存修改密码
	 * @param passwordOld
	 * @param passwordNew
	 * @param user
	 */
	ServerResponse<String> resetPassword(String passwordOld,String passwordNew,User user);
	
    /**
     * 判断用户是否为管理员且其已登录
     * @param user
     * @return
     */
	ServerResponse checkAdminRole(User user);
}
