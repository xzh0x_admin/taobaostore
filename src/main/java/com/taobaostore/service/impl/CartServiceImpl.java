package com.taobaostore.service.impl;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.Spliterator;

import org.apache.ibatis.javassist.compiler.ast.NewExpr;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.taobaostore.common.BigDecimalUtil;
import com.taobaostore.common.Const;
import com.taobaostore.common.ResponseCode;
import com.taobaostore.common.ServerResponse;
import com.taobaostore.dao.CartMapper;
import com.taobaostore.dao.ProductMapper;
import com.taobaostore.pojo.Cart;
import com.taobaostore.pojo.Product;
import com.taobaostore.service.ICartService;
import com.taobaostore.util.DateTimeUtil;
import com.taobaostore.util.PropertiesUtil;
import com.taobaostore.vo.CartProductVo;
import com.taobaostore.vo.CartVo;
@Service
@Primary
public class CartServiceImpl implements ICartService{
	@Autowired
	private CartMapper cartMapper;
	@Autowired
	private ProductMapper productMapper;

	/**
	 * 查询该用户下所有购物车信息
	 * @param userId
	 * @return
	 */
	public ServerResponse<CartVo> list(Integer userId) {
		CartVo cartVo = this.getCartVoLimit(userId);
		return ServerResponse.createBySuccess(cartVo);
	}

	
	/**
	 * 新增购物车记录
	 */
	public ServerResponse<CartVo> add(Integer userId,Integer productId,Integer count) {
		if (productId == null || count == null) {
			return ServerResponse.createByError(ResponseCode.ILLEGAL_ARGUMENT.getCODE(), ResponseCode.ILLEGAL_ARGUMENT.getDESC());
		}
//		System.out.println("测试"+cartMapper.selectCartByUserIdProductId(userId, productId));
		Cart cart = cartMapper.selectCartByUserIdProductId(userId, productId);
		if (cart == null) {
			//这个购物车记录不存在
			Cart cartItem = new Cart();
			cartItem.setQuantity(count);
			cartItem.setChecked(Const.Cart.ON_CHECKED);
			cartItem.setProductId(productId);
			cartItem.setUserId(userId);
			cartMapper.insert(cartItem);
		}else {
			//如果已经存在这个商品的记录,就叠加他的数量
			count = cart.getQuantity() + count;
			cart.setQuantity(count);
			cartMapper.updateByPrimaryKeySelective(cart);
		}
		CartVo cartVo = this.getCartVoLimit(userId);
		return ServerResponse.createBySuccess("新增购物车记录成功!", cartVo);
	}

	/**
	 * 更新购物车
	 * @param userId
	 * @param productId
	 * @param count
	 * @return
	 */
	public ServerResponse<CartVo> update(Integer userId,Integer productId,Integer count){
		if (productId == null || count == null) {
			return ServerResponse.createByError(ResponseCode.ILLEGAL_ARGUMENT.getCODE(), ResponseCode.ILLEGAL_ARGUMENT.getDESC());
		}
		Cart cart = cartMapper.selectCartByUserIdProductId(userId, productId);
		if (cart != null) {
			cart.setQuantity(count);
		}
		cartMapper.updateByPrimaryKeySelective(cart);
		CartVo cartVo = this.getCartVoLimit(userId);
		return ServerResponse.createBySuccess(cartVo);
	}
	
	/***
	 * 删除购物车记录
	 * @param userId
	 * @param productIds
	 * @return
	 */
	public ServerResponse<CartVo> deleteProduct(Integer userId,String productIds){
		List<String> list = Splitter.on(",").splitToList(productIds);
		if (null == list || list.size() == 0) {
			return ServerResponse.createByError(ResponseCode.ILLEGAL_ARGUMENT.getCODE(), ResponseCode.ILLEGAL_ARGUMENT.getDESC());
		}
		cartMapper.deleteByUserIdProductIds(userId, list);
		CartVo cartVo = this.getCartVoLimit(userId);
		return ServerResponse.createBySuccess(cartVo);
	}
	
	
	/**
	 * 全选购物车商品
	 * @return
	 */
	public ServerResponse<CartVo> checkedAll(Integer userId){
		if (null == userId) {
			return ServerResponse.createByError(ResponseCode.ILLEGAL_ARGUMENT.getCODE(), ResponseCode.ILLEGAL_ARGUMENT.getDESC());
		}
		cartMapper.updateByCheckPitchAll(userId);
		return ServerResponse.createBySuccess(this.getCartVoLimit(userId));
	}
	/**
	 * 全反选购物车商品
	 * @return
	 */
	public ServerResponse<CartVo> UncheckedAll(Integer userId){
		if (null == userId) {
			return ServerResponse.createByError(ResponseCode.ILLEGAL_ARGUMENT.getCODE(), ResponseCode.ILLEGAL_ARGUMENT.getDESC());
		}
		cartMapper.updateByCheckInvertAll(userId);
		return ServerResponse.createBySuccess(this.getCartVoLimit(userId));
	}
	/**
	 * 选中某个商品
	 * @param userId
	 * @param productId
	 * @return
	 */
	public ServerResponse<CartVo> checkPitch(Integer userId,Integer productId){
		if (null == userId || null == productId) {
			return ServerResponse.createByError(ResponseCode.ILLEGAL_ARGUMENT.getCODE(), ResponseCode.ILLEGAL_ARGUMENT.getDESC());
		}
	     cartMapper.updateByCheckPitch(userId, productId);
	     return ServerResponse.createBySuccess(this.getCartVoLimit(userId));
	}
	
	/**
	 * 反选某个商品
	 * @param userId
	 * @return
	 */
	public ServerResponse<CartVo> UncheckPitch(Integer userId,Integer productId){
		if (null == userId || null == productId) {
			return ServerResponse.createByError(ResponseCode.ILLEGAL_ARGUMENT.getCODE(), ResponseCode.ILLEGAL_ARGUMENT.getDESC());
		}
		int resultNum = cartMapper.updateByCheckInvert(userId, productId);
		return ServerResponse.createBySuccess(this.getCartVoLimit(userId));
	}
	
	/**
	 * 用于校验库存等问题并封装VO对象返回使用
	 * @param userId
	 * @return
	 */
	private CartVo getCartVoLimit(Integer userId) {
		CartVo cartVo = new CartVo();
		// 获取这个用户下的所有购物车商品记录
		List<Cart> cartList = cartMapper.selectCartByUserId(userId);
		List<CartProductVo> cartProductVoList = Lists.newArrayList();
		// 初始化商品总价格
		BigDecimal cartTotalPrice = new BigDecimal("0");

		if (cartList != null && cartList.size() > 0) {
			// 遍历cartList 用于填充cartProductVoList
			for (Cart cart : cartList) {
//				System.out.println(cart);
				CartProductVo cartProductVo = new CartProductVo();
				cartProductVo.setId(cart.getId());
				cartProductVo.setUserId(userId);
				cartProductVo.setProductId(cart.getProductId());

//				System.out.println("要搜索的商品:"+cart.getProductId());
				Product product = productMapper.getProductById(cart.getProductId());
				if (product != null) {
					cartProductVo.setProductMainImage(product.getMainImage());
					cartProductVo.setProductName(product.getName());
					cartProductVo.setProductSubtitle(product.getSubtitle());
					cartProductVo.setProductStatus(product.getStatus());
					cartProductVo.setProductPrice(product.getPrice());
					//判断库存是否合法
					int buyLimitCount = 0;
					if (product.getStock() >= cart.getQuantity()) {
						// 在库存充足的时候为我们要买的数量
						buyLimitCount = cart.getQuantity();
						cartProductVo.setLimitQuantity(Const.Cart.LIMIT_NUM_SUCCESS);
					}else {
						// 库存不充足的时候直接改为最大库存数量
						buyLimitCount = product.getStock();
						cartProductVo.setLimitQuantity(Const.Cart.LIMIT_NUM_FAIL);
						//购物车中更新有效库存
						Cart cartForQuantity = new Cart();
						cartForQuantity.setId(cart.getId());
						cartForQuantity.setQuantity(buyLimitCount);
						cartMapper.updateByPrimaryKeySelective(cartForQuantity);
					}
					cartProductVo.setQuantity(buyLimitCount);
					// 计算当前商品总价格
					cartProductVo.setProductTotalPrice(BigDecimalUtil.mul(product.getPrice().doubleValue(), cartProductVo.getQuantity().doubleValue()));
					cartProductVo.setProductChecked(cart.getChecked());
				}
				if (cart.getChecked() == Const.Cart.CHECKED) {
					// 如果被选中就添加整个购物车总价
					cartTotalPrice = BigDecimalUtil.add(cartTotalPrice.doubleValue(), cartProductVo.getProductTotalPrice().doubleValue());
				}
				cartProductVoList.add(cartProductVo);
			}
		}
//		System.out.println("总价格"+cartTotalPrice);
		cartVo.setCartTotalPrice(cartTotalPrice);
		cartVo.setCartProductVoList(cartProductVoList);
		cartVo.setAllChecked(this.getAllCheckedStatus(userId));// 是否全选
		cartVo.setImageHost(PropertiesUtil.getValue("FTP_IP"));
		return cartVo;
	}

	/**
	 * 判断这个用户下的购物车是否全选状态
	 * @param userId
	 * @return
	 */
	private boolean getAllCheckedStatus(Integer userId) {
		if (userId == null) {
			return false;
		}
		return cartMapper.selectCartProductCheckedStatusByUserId(userId)==0;
	}
}
