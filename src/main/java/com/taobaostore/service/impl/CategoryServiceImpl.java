package com.taobaostore.service.impl;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Sets;
import com.taobaostore.common.ServerResponse;
import com.taobaostore.dao.CategoryMapper;
import com.taobaostore.pojo.Category;
import com.taobaostore.service.ICategoryService;

@Primary
@Transactional
@Service
public class CategoryServiceImpl implements ICategoryService {
	
	@Autowired
    private CategoryMapper categorymapper;
	
    /**
     * 根据品类父id返回指定品类列表(查询同一类型的品类信息)
     * @param parent_id
     * @return
     */
	public ServerResponse<List<Category>> listCategoryByParentId(int parent_id){
		List<Category> list = categorymapper.listCategoryByParentId(parent_id);
		if (null != list && list.size()!=0) {
			return ServerResponse.createBySuccess(list);
		}
		return ServerResponse.createByError("没有找到数据");
	}
	
	/**
	 * 新增分类
	 * @param categoryName
	 * @param parentId
	 * @return
	 */
	@Override
	public ServerResponse addCategory(String categoryName, Integer parentId) {
		if (parentId == null || StringUtils.isBlank(categoryName)) {
			return ServerResponse.createByError("添加类型参数错误!");
		}
		Category category = new Category();
		category.setName(categoryName);
		category.setParentId(parentId);
		category.setStatus(true);
		int rowCount = categorymapper.insert(category);
		if (rowCount > 0) {
			return ServerResponse.createBySuccess("添加分类成功");
		}
		return ServerResponse.createBySuccess("添加分类成功!");
	}
	
    /**
     * 更新分类名称
     * @param categoryId
     * @param categoryName
     * @return
     */
	@Override
	public ServerResponse updateCategory(Integer categoryId, String categoryName) {
		if (null == categoryId || StringUtils.isBlank(categoryName)) {
			return ServerResponse.createByError("参数为空");
		}
	    Category category = new Category();
	    category.setId(categoryId);
	    category.setName(categoryName);
	    
	    int rowCount = categorymapper.updateByPrimaryKeySelective(category);
	    if (rowCount > 0) {
			return ServerResponse.createBySuccess("更新分类成功!");
		}
		return ServerResponse.createByError("更新分类失败");
	}
	
	/**
	 * 获取指定分类下的分类(不递归)
	 */
	public ServerResponse getChildrenParallelCategroy(Integer categoryId) {
		if (null == categoryId) {
			return ServerResponse.createByError("参数为空");
		}
		List<Category> list = categorymapper.listCategoryChildren(categoryId);
		if (list.isEmpty()) {
			return ServerResponse.createByError("没有找到分类!");
		}
		return ServerResponse.createBySuccess(list);
	}
	/**
	 * 获取指定分类下的所有子分类(递归算法)
	 */
	public ServerResponse<List<Integer>> selectCategoryAndDeepChildrenByrenId(Integer categoryId) {
		if (null == categoryId) {
			return ServerResponse.createByError("参数为空");
		}
		// 获取这个要递归的品类
		Set<Category> CategorySets = Sets.newConcurrentHashSet();
		findChildCategory(CategorySets, categoryId);
		System.out.println("元素数量："+CategorySets.size());
		List<Integer> list =new ArrayList<Integer>();
		for (Category categoryItem : CategorySets) {
			list.add(categoryItem.getId());
		}
		return ServerResponse.createBySuccess("读取子分类成功", list);
	}
	/**
	 * 递归节点
	 * @param categorytSet
	 * @param categoryId
	 * @return
	 */
	private Set<Category> findChildCategory(Set<Category> categorytSet,Integer categoryId){
		// 获取当前节点信息(判断这个节点是否还存在)
		Category category = categorymapper.getCategoryById(categoryId);
		// 这个节点如果不是空的我们就添加进集合
		if (category != null) {
			categorytSet.add(category);
		}
		// 查询出这个节点下的所有子节点(非递归)
		List<Category> list = categorymapper.listCategoryChildren(categoryId);
	    for (Category categoryItem : list) {
			// 将每一个子节点再递归入这个方法 判断其是否有子节点
	    	findChildCategory(categorytSet, categoryItem.getId());
		}
		return categorytSet;
	}

	@Override
	public ServerResponse<List<Category>> listCategory() {
		List<Category> list = categorymapper.listCategory();
		System.out.println("dao"+list);
		if (null!=list&&list.size()>0) {
			return ServerResponse.createBySuccess(list);
		}
		return ServerResponse.createByError("没有找到数据");
	}
}