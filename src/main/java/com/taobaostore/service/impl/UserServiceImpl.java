package com.taobaostore.service.impl;


import java.util.Date;
import java.util.UUID;


import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.taobaostore.common.AliyunOSSUtil;
import com.taobaostore.common.Const;
import com.taobaostore.common.ServerResponse;
import com.taobaostore.common.TokenCache;
import com.taobaostore.dao.UserMapper;
import com.taobaostore.pojo.User;
import com.taobaostore.service.IFileService;
import com.taobaostore.service.IUserService;
import com.taobaostore.util.FTPUtil;
import com.taobaostore.util.MD5Util;
import com.taobaostore.util.StringUtil;
import com.taobaostore.vo.ProductListVo;
@Service
@Primary
public class UserServiceImpl implements IUserService{

	@Autowired
	private UserMapper userMapper;
	@Autowired
	private IFileService iFileService;

	/**
	 * 用户登录
	 */
	@Override
	public ServerResponse<User> login(String username, String password) {
		System.out.println(username);
		username = StringUtil.getEncoding(username);
		System.out.println(username);
		// 验证用户是否存在
		if (userMapper.checkUserName(username)==0) {
			return ServerResponse.createByError("用户名不存在");
		}
		// 密码登录(MD5)
		password = MD5Util.MD5Encode(password);
		User user = userMapper.selectLogin(username, password);
		if (user == null) {
			return ServerResponse.createByError("密码错误");
		}
		// 将用户密码置空(保证安全)
		user.setPassword(StringUtils.EMPTY);
		System.out.println(user.getHeadpic());
		return ServerResponse.createBySuccess("登录成功", user);
	}


	/**
	 * 用户信息注册
	 * @param user
	 * @return
	 */
	public ServerResponse<String> register(User user){
		// 对用户的中文进行强制编码
		user = StringUtil.getEncodingUser(user);
		System.out.println(user);
		// 对用户信息进行校验
		ServerResponse<String> response = this.checkValid(Const.USER_NAME, user.getUsername());
		if(!response.isSuccess()) return response;
		response = this.checkValid(Const.EMAIL, user.getEmail());
		if(!response.isSuccess()) return response;
		// 设置用户时间轴
		user.setCreateTime(new Date());
		user.setUpdateTime(new Date());
		// 设置用户默认权限
		user.setRole(Const.Role.ROLE_USER);
		// 将用户密码进行MD5加密
		user.setPassword(MD5Util.MD5Encode(user.getPassword()));
		user.setHeadpic("http://49.232.168.42/taobaoHead.jpg");
		System.out.println("注册成功要看看");
		userMapper.insert(user);
		return ServerResponse.createBySuccess("用户注册成功!");
	}


	/**
	 * 校验用户信息
	 * @param type
	 * @param str
	 * @return
	 */
	public ServerResponse<String> checkValid(String type,String str){
		// 判断类型不为null且不为空字符串的情况下进行校验
		if (StringUtils.isNotBlank(type)) {
			// 判断类型进行对应的校验
			if (Const.USER_NAME.equals(type)) {
				if (userMapper.checkUserName(str) > 0) {
					return ServerResponse.createByError("用户已存在!");
				}
			}
			if (Const.EMAIL.equals(type)) {
				if (userMapper.checkEmail(str) > 0) {
					return ServerResponse.createByError("邮箱已被使用!");
				}
			}
		}else {
			return ServerResponse.createByError("参数错误!");
		}
		return ServerResponse.createBySuccess("校验成功!");
	}


	/**
	 * 验证用户是否有密保问题
	 * 如果有就返回其密保问题
	 * @param username
	 * @return
	 */
	public ServerResponse<String> selectQuestion(String username){
		username = StringUtil.getEncoding(username);
		ServerResponse<String> response = this.checkValid(Const.USER_NAME,username);
		if (response.isSuccess()) {
			// 如果用户不存在
			return ServerResponse.createByError("用户不存在!");
		}
		String question = userMapper.selectQuestion(username);

		if (!StringUtils.isNotBlank(question)) {
			// 如果密保问题等于空的话
			return ServerResponse.createByError("没有密保问题!");
		}
		return ServerResponse.createBySuccess(question);
	}


	/**
	 *查询指定用户的指定密保问题是否正确
	 * @param username
	 * @param question
	 * @param answer
	 * @return
	 */
	public ServerResponse<String> checkQuestion(String username,String question,String answer){
		username = StringUtil.getEncoding(username);
		question = StringUtil.getEncoding(question);
		answer = StringUtil.getEncoding(answer);
		System.out.println("信息:"+username+"-"+question+"-"+answer);
		int count = userMapper.selectAnswer(username, question, answer);
		System.out.println("查询测试"+count);
		if (count > 0) {
			// 说明验证成功,将这个记录存入缓存
			String token = UUID.randomUUID().toString();
			TokenCache.setKey(TokenCache.TOKEN_NAME+username,token);
			return ServerResponse.createBySuccess(token);
		}else {
			// 如果再次验证的时候失败了 那就清除token
			TokenCache.setKey(TokenCache.TOKEN_NAME+username,StringUtils.EMPTY);
		}
		return ServerResponse.createByError("答案错误,请重新输入!");
	}

	/**
	 * 忘记密码的情况下修改密码
	 * @return
	 */
	public ServerResponse<String> forgetResetPassword(String username,String passwordNew,String forgetToken){
		username = StringUtil.getEncoding(username);
		if (this.checkValid(Const.USER_NAME, username).isSuccess()) {
			return ServerResponse.createByError("用户不存在");
		}
		forgetToken = TokenCache.getKey(TokenCache.TOKEN_NAME+username);
		if (StringUtils.isBlank(forgetToken)) {
			// token不存在或者被销毁
			return ServerResponse.createByError("token不存在!");
		}
		System.out.println("当前token:"+forgetToken);
		// 如果token存在即说明已经通过密保验证
		// 将用户的新密码转换成md5格式
		String MD5Password = MD5Util.MD5Encode(passwordNew);
		userMapper.updatePwd(username, MD5Password);
		return ServerResponse.createBySuccess("密码找回成功!");
	}  


	/**
	 * 修改用户信息
	 * @param user
	 * @return
	 */
	public ServerResponse<User> updateUserMsg(User user,MultipartFile pic){
		try {
			if (null!=pic.getOriginalFilename() && !pic.getOriginalFilename().trim().equals("")) {
				//				// 获取上传的图片的后缀名
				String imgSuffix = pic.getOriginalFilename().substring(pic.getOriginalFilename().lastIndexOf("."));
				// 上传图片并设置图片信息
				//	Files.copy(pic.getInputStream(), Paths.get(Const.USER_PIC,imgname));
				String fileName = UUID.randomUUID().toString().replace("==", "");
				//	ServerResponse<String> response = iFileService.upload(pic,Const.USER_PIC);
				// 文件名 = 文件名 + 后缀
				// 为什么要重新拼接文件后缀而不是直接把原本的文件名丢进来呢？
				// 因为要保证文件名的唯一性，所以使用UUID生成+原文件名后缀
				AliyunOSSUtil.fileUpload(fileName + imgSuffix, pic.getInputStream());	
				user.setHeadpic(AliyunOSSUtil.aliyunOSSURL + "/"+ AliyunOSSUtil.objectNamePrefix + "/" +fileName + imgSuffix);
				System.out.println(user.getHeadpic());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		System.out.println("图片路径:"+user.getHeadpic());
		System.out.println(user);
		if (null == user || null == user.getUsername()) {
			return ServerResponse.createByError("没有找到要修改的用户信息!");
		}
		// 更新用户信息
		userMapper.updateByPrimaryKeySelective(user);
		// 更新完用户信息之后将更新的图片从固定目录写入动态目录用于读取
		// 如果没有上传头像那就使用默认的
		if (null==pic.getOriginalFilename() || pic.getOriginalFilename().trim().equals("")) {
			user.setHeadpic("http://47.93.114.228/taobaoHead.jpg");
			user.setUpdateTime(new Date());
		}
		ServerResponse<User> response = ServerResponse.createBySuccess("用户信息更新成功!", user);
		return response;
	}


	/**
	 * 在登录情况下修改密码
	 * @param passwordOld
	 * @param passwordNew
	 * @param user
	 * @return
	 */
	public ServerResponse<String> resetPassword(String passwordOld,String passwordNew,User user){
		// 防止横向越权:在修改密码的时候要校验旧密码,并且判断其id
		if (userMapper.selectPassword(MD5Util.MD5Encode(passwordOld), user.getId()) == 0) {
			// 旧密码错误
			return ServerResponse.createByError("旧密码错误");
		}
		// 设置新密码
		user.setPassword(MD5Util.MD5Encode(passwordNew));
		int updateCount = userMapper.updateByPrimaryKeySelective(user);
		if (updateCount > 0) {
			// 密码更新成功
			return ServerResponse.createBySuccess("密码更新成功");
		}
		return ServerResponse.createBySuccess("密码更新失败");
	}

	/**
	 * 判断用户是否登录且为管理员权限
	 * @param user
	 * @return
	 */
	public ServerResponse checkAdminRole(User user) {
		if (null != user && user.getRole() == Const.Role.ROLE_ADMIN) {
			return ServerResponse.createBySuccess();
		}
		return ServerResponse.createByError();
	}
}
