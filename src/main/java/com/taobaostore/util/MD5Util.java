package com.taobaostore.util;

import java.security.MessageDigest;

import sun.misc.BASE64Encoder;

public class MD5Util {
	public static String MD5Encode(String str) {
		String result = null;
		try {
			str = new StringBuffer().append(str).append("").toString();
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            BASE64Encoder encoder = new BASE64Encoder();
            result = encoder.encode(md5.digest(str.getBytes("UTF-8"))).replace("==", "");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return result.toUpperCase();
	}
}
