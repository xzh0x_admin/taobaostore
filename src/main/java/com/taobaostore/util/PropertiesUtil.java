package com.taobaostore.util;

import java.io.IOException;
import java.util.Properties;

import org.apache.commons.lang3.StringUtils;

public class PropertiesUtil {
    private static Properties properties;
    static {
    	properties = new Properties();
    	try {
			properties.load(Thread.currentThread().getContextClassLoader().getResourceAsStream("ftpDataSource.properties"));
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
    /**
     * 返回指定key的value 如果value为空则返回null
     * @param key
     * @return
     */
    public static String getValue(String key) {
    	String value = properties.getProperty(key.trim());
    	if (StringUtils.isBlank(value)) {
			return null;
		}
    	return value.trim();
    }
    /**
     * 设置默认值的获取value方式
     * @param key
     * @param defaultValue
     * @return
     */
    public static String getValue(String key,String defaultValue) {
    	String value = properties.getProperty(key.trim());
    	if (StringUtils.isBlank(value)) {
			return defaultValue;
		}
    	return value.trim();
    }
}
