package com.taobaostore.util;

import java.io.UnsupportedEncodingException;

import com.taobaostore.pojo.User;

public class StringUtil {
	private static final String ENCODING = "UTF-8";

	public static String getEncoding(String str) {
		try {
			return new String(str.getBytes("ISO-8859-1"),ENCODING);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 将user对象中需要编码的都进行一次编码
	 * @param user
	 * @return
	 */
	public static User getEncodingUser(User user) {
		// 对用户的中文进行强制编码
		user.setAnswer(StringUtil.getEncoding(user.getAnswer()));
		user.setQuestion(StringUtil.getEncoding(user.getQuestion()));
		user.setUsername(StringUtil.getEncoding(user.getUsername()));
		return user;
	}
}
