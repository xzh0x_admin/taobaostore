package com.taobaostore.vo;

import java.math.BigDecimal;
import java.util.List;
//封装整个用户的购物车信息
public class CartVo {

    private List<CartProductVo> cartProductVoList;//购物车下所有子购物车记录
    private BigDecimal cartTotalPrice;//商品总价格
    private Boolean allChecked;//是否已经都勾选
    private String imageHost;// 图片路径

    public List<CartProductVo> getCartProductVoList() {
        return cartProductVoList;
    }

    public void setCartProductVoList(List<CartProductVo> cartProductVoList) {
        this.cartProductVoList = cartProductVoList;
    }

    public BigDecimal getCartTotalPrice() {
        return cartTotalPrice;
    }

    public void setCartTotalPrice(BigDecimal cartTotalPrice) {
        this.cartTotalPrice = cartTotalPrice;
    }

    public Boolean getAllChecked() {
        return allChecked;
    }

    public void setAllChecked(Boolean allChecked) {
        this.allChecked = allChecked;
    }

    public String getImageHost() {
        return imageHost;
    }

    public void setImageHost(String imageHost) {
        this.imageHost = imageHost;
    }

}
