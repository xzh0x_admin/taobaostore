package com.taobaostore.vo;

import java.math.BigDecimal;
import java.util.List;

public class OrderProductVo {
	private List<OrderItemVo> orderItemVoList;// 所有子订单
	private BigDecimal productTotalPrice;// 商品总价格
	private String imageHost;// 图片服务器前缀
	
	public List<OrderItemVo> getOrderItemVoList() {
		return orderItemVoList;
	}
	public void setOrderItemVoList(List<OrderItemVo> orderItemVoList) {
		this.orderItemVoList = orderItemVoList;
	}
	public BigDecimal getProductTotalPrice() {
		return productTotalPrice;
	}
	public void setProductTotalPrice(BigDecimal productTotalPrice) {
		this.productTotalPrice = productTotalPrice;
	}
	public String getImageHost() {
		return imageHost;
	}
	public void setImageHost(String imageHost) {
		this.imageHost = imageHost;
	}

}
