package com.taobaostore.vo;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

public class OrderVo {
	private Integer id;// 订单表id

	private Long orderNo;// 订单号

	private Integer userId;// 用户id

	private Integer receiverId;// 送货地址id

	private BigDecimal payment;// 实际付款金额,单位是元,保留两位小数

	private Integer paymentType;// 付款方式(在线支付)
	
	private String paymentTypeDesc;// 付款方式(类型)

	private Integer postage;// 运费(元)

	private Integer status;// 订单状态(订单状态:0-取消,10-未付款,20-已付款,40-已发货,50-交易成功,60-交易关闭)

	private Date paymentTime;// 支付时间
	
	private ReceiverVo receiverVo;//收货信息
	
	private List<OrderItemVo> listOrderItemVo;// 其订单子项记录

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Long getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(Long orderNo) {
		this.orderNo = orderNo;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getReceiverId() {
		return receiverId;
	}

	public void setReceiverId(Integer receiverId) {
		this.receiverId = receiverId;
	}

	public BigDecimal getPayment() {
		return payment;
	}

	public void setPayment(BigDecimal payment) {
		this.payment = payment;
	}

	public Integer getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(Integer paymentType) {
		this.paymentType = paymentType;
	}

	public Integer getPostage() {
		return postage;
	}

	public void setPostage(Integer postage) {
		this.postage = postage;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Date getPaymentTime() {
		return paymentTime;
	}

	public void setPaymentTime(Date paymentTime) {
		this.paymentTime = paymentTime;
	}

	public ReceiverVo getReceiverVo() {
		return receiverVo;
	}

	public void setReceiverVo(ReceiverVo receiverVo) {
		this.receiverVo = receiverVo;
	}

	public List<OrderItemVo> getListOrderItemVo() {
		return listOrderItemVo;
	}

	public void setListOrderItemVo(List<OrderItemVo> listOrderItemVo) {
		this.listOrderItemVo = listOrderItemVo;
	}

	public String getPaymentTypeDesc() {
		return paymentTypeDesc;
	}

	public void setPaymentTypeDesc(String paymentTypeDesc) {
		this.paymentTypeDesc = paymentTypeDesc;
	}
	
	@Override
	public String toString() {
		return "OrderVo [id=" + id + ", orderNo=" + orderNo + ", userId=" + userId + ", receiverId=" + receiverId
				+ ", payment=" + payment + ", paymentType=" + paymentType + ", paymentTypeDesc=" + paymentTypeDesc
				+ ", postage=" + postage + ", status=" + status + ", paymentTime=" + paymentTime + ", receiverVo="
				+ receiverVo + ", listOrderItemVo=" + listOrderItemVo + "]";
	}
}
