package com.taobaostore.vo;

import java.math.BigDecimal;

public class ProductDetailVo {
	private Integer id;// 商品id
	private Integer categoryId;//分类id
	private String name;//商品名称
	private String detail;// 商品详情
	private String subtitle;// 商品子标题
	private String mainImage;//主图片
	private String[] subImages;//副图片
	private BigDecimal price;//价格
	private Integer stock;//商品库存
	private Integer status;//商品状态
	private String createTime;//创建时间
	private String updateTime;//更新时间

	private String imageHost;//获取图片的路径
	private Integer parentCategoryId;//分类id的父id
	
	
	public String getDetail() {
		return detail;
	}
	public void setDetail(String detail) {
		this.detail = detail;
	}
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getCategoryId() {
		return categoryId;
	}
	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSubtitle() {
		return subtitle;
	}
	public void setSubtitle(String subtitle) {
		this.subtitle = subtitle;
	}
	public String getMainImage() {
		return mainImage;
	}
	public void setMainImage(String mainImage) {
		this.mainImage = mainImage;
	}
	
	public String[] getSubImages() {
		return subImages;
	}
	public void setSubImages(String[] subImages) {
		this.subImages = subImages;
	}
	public BigDecimal getPrice() {
		return price;
	}
	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	public Integer getStock() {
		return stock;
	}
	public void setStock(Integer stock) {
		this.stock = stock;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getCreateTime() {
		return createTime;
	}
	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}
	public String getUpdateTime() {
		return updateTime;
	}
	public void setUpdateTime(String updateTime) {
		this.updateTime = updateTime;
	}
	public String getImageHost() {
		return imageHost;
	}
	public void setImageHost(String imageHost) {
		this.imageHost = imageHost;
	}
	public Integer getParentCategoryId() {
		return parentCategoryId;
	}
	public void setParentCategoryId(Integer parentCategoryId) {
		this.parentCategoryId = parentCategoryId;
	}
	@Override
	public String toString() {
		return "ProductDetailVo [id=" + id + ", categoryId=" + categoryId + ", name=" + name + ", subtitle=" + subtitle
				+ ", mainImage=" + mainImage + ", subImages=" + subImages + ", price=" + price + ", stock=" + stock
				+ ", status=" + status + ", createTime=" + createTime + ", updateTime=" + updateTime + ", imageHost="
				+ imageHost + ", parentCategoryId=" + parentCategoryId + "]";
	}
}
