package com.taobaostore.vo;

import java.util.Date;

public class ProductListVo {
	private Integer id;// 商品id
	
	private Integer categoryId;// 商品种类(对应其ID)
	
	private String categoryName;// 商品类型名称
	
	private String name;// 商品名
	
	private Integer stock;// 库存数量
	
	private Integer status;// 商品状态码
	
	private String statusName;// 商品状态(1-在售,2-下架,3-删除)

	private String createTime;// 創建時間

	private String image_Main;//商品主图
	
	
	public String getImage_Main() {
		return image_Main;
	}

	public void setImage_Main(String image_Main) {
		this.image_Main = image_Main;
	}

	public String getCategoryName() {
		return categoryName;
	}

	public void setCategoryName(String categoryName) {
		this.categoryName = categoryName;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(Integer categoryId) {
		this.categoryId = categoryId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getStock() {
		return stock;
	}

	public void setStock(Integer stock) {
		this.stock = stock;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}

	

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	@Override
	public String toString() {
		return "ProductListVo [id=" + id + ", categoryId=" + categoryId + ", categoryName=" + categoryName + ", name="
				+ name + ", stock=" + stock + ", status=" + status + ", statusName=" + statusName + ", createTime="
				+ createTime + ", image_Main=" + image_Main + "]";
	}
	
}
