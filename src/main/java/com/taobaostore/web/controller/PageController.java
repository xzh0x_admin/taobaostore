package com.taobaostore.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
// 用于做页面控制
@Controller
public class PageController {
	// 主页面
	@RequestMapping("index")
	public String pageindex(Model model) {
		System.out.println(1);
		return "index";
	}
	// 登录页面
	@RequestMapping("Land")
	public String pageLand() {
		return "Land";
	}
	// 用户信息
	@RequestMapping("userMsg")
	public String pageUserMsg(){
		return "userMsg";
	}
	// 注册页面
	@RequestMapping("register")
	public String pageRegister() {
		return "register";
	}
	// 购物车
	@RequestMapping("Shopping_Cart")
	public String pageShopping_Cart() {
		return "Shopping_Cart";
	}

	// 用户信息更新页
	@RequestMapping("userMsg_Update")
	public String pageuserMsg_Update(){
		return "userMsg_Update";
	}

	// 订单信息
	@RequestMapping("orderMsg")
	public String pageOrderMsg() {
		return "orderMsg.html";
	}

	// 关于涛宝商城
	@RequestMapping("taobaostoreMsg")
	public String pagetaobaostoreMsg() {
		return "taobaostoreMsg";
	}

	// 登录情况下修改密码
	@RequestMapping("retrievePwd")
	public String pageretrievePwd() {
		return "retrievePwd";
	}

	// 找回密码
	@RequestMapping("findPwd")
	public String pagefindPwd() {
		return "findPwd";
	}

	// 管理员页面
	@RequestMapping("admin")
	public String pageManage() {
		return "admin";
	}

	// 管理员登录页面
	@RequestMapping("adminlogin")
	public String pageManageLogin() {
		return "adminlogin";
	}

	// 文件上傳測試
	@RequestMapping("uploaddemo")
	public String pageuploadDemo() {
		return "uploaddemo";
	}

	// 商品更新页面
	@RequestMapping("updateproduct")
	public String pageupdateproduct() {
		return "updateproduct";
	}

	// 商品更新页面
	@RequestMapping("serachProduct")
	public String pageserachProduct() {
		return "serachProduct";
	}

	// 商品详情页面
	@RequestMapping("product_Msg")
	public String pageproduct_Msg() {
		return "product_Msg";
	}

	// 确认订单信息
	@RequestMapping("order_affirm")
	public String orderMsg() {
		return "order_affirm";
	}
	
	// 下单页
	@RequestMapping("orderPay")
	public String orderPay() {
		return "orderPay";
	}
}