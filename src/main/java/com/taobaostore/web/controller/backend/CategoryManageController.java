package com.taobaostore.web.controller.backend;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.ibatis.annotations.Param;
import org.aspectj.apache.bcel.generic.RET;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.taobaostore.common.Const;
import com.taobaostore.common.ResponseCode;
import com.taobaostore.common.ServerResponse;
import com.taobaostore.pojo.Category;
import com.taobaostore.pojo.User;
import com.taobaostore.service.ICategoryService;
import com.taobaostore.service.IUserService;

@Controller
@RequestMapping("/manage/category")
public class CategoryManageController {
	@Autowired
	private IUserService iUserService;

	@Autowired
	private ICategoryService iCategoryService;
	
	/**
	 * 返回指定类型品类的所有列表记录
	 * @param session
	 * @param parent_id
	 * @return
	 */
	@RequestMapping("listCategoryByParentId.do")
	@ResponseBody
	public ServerResponse<List<Category>> listCategoryByParentId(HttpSession session,
			@RequestParam(value = "parent_id",defaultValue = "0")Integer parent_id){
		User user = (User)session.getAttribute(Const.CURRENT_USER);
		if (null == user) {
			return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getCODE(), "用户未登录!");
		}
		if (iUserService.checkAdminRole(user).isSuccess()) {
			// 是管理员 执行增加分类逻辑
			return iCategoryService.listCategoryByParentId(parent_id);
		}
		return ServerResponse.createByError("无权限操作,需要管理员权限!");
	}
	
    /**
     * 分类新增节点
     * @param session
     * @param categoryName
     * @param parentId
     * @return
     */
	@RequestMapping("addCategory.do")
	@ResponseBody
	public ServerResponse addCategory(HttpSession session,String categoryName,@RequestParam(value = "parentId",defaultValue = "0")int parentId) {
		User user = (User)session.getAttribute(Const.CURRENT_USER);
		if (null == user) {
			return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getCODE(), "用户未登录!");
		}
		if (iUserService.checkAdminRole(user).isSuccess()) {
			// 是管理员 执行增加分类逻辑
			return iCategoryService.addCategory(categoryName, parentId);
		}else {
			// 不是管理员
			return ServerResponse.createByError("无权限操作,需要管理员权限!");
		}
	}
	
	/**
	 * 更新分类节点
	 * @param session
	 * @param categoryId
	 * @param categoryName
	 * @return
	 */
	@RequestMapping("updateCategory.do")
	@ResponseBody
	public ServerResponse updateCategory(HttpSession session,Integer categoryId,String categoryName) {
		User user = (User)session.getAttribute(Const.CURRENT_USER);
		if (null == user) {
			return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getCODE(), "用户未登录!");
		}
		if (iUserService.checkAdminRole(user).isSuccess()) {
			// 是管理员权限,更新
			return iCategoryService.updateCategory(categoryId, categoryName);
		}
		return ServerResponse.createByError("更新失败");
	}
	
	/**
	 * 查询指定节点下的子节点
	 * @param session
	 * @param categoryId
	 * @return
	 */
	@RequestMapping("getChildrenParallelCategroy.do")
	@ResponseBody
	public ServerResponse getChildrenParallelCategroy(HttpSession session,@RequestParam(value = "categoryId",defaultValue = "0")Integer categoryId) {
		User user = (User)session.getAttribute(Const.CURRENT_USER);
		if (null == user) {
			return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getCODE(), "用户未登录!");
		}
		if (iUserService.checkAdminRole(user).isSuccess()) {
			// 如果是管理员开始执行操作
			// 返回不递归的同级子节点
			return iCategoryService.getChildrenParallelCategroy(categoryId);
		}else {
			return ServerResponse.createByError("没有权限操作,需要管理员权限!");
		}
	}
	
	/**
	 * 查询指定分类下的所有子分类的id
	 * @param session
	 * @param categoryId
	 * @return
	 */
	@RequestMapping("getCategoryAndDeepChildrenCategory.do")
	@ResponseBody
	public ServerResponse getCategoryAndDeepChildrenCategory(HttpSession session,@RequestParam(value = "categoryId",defaultValue = "0")Integer categoryId) {
		User user = (User)session.getAttribute(Const.CURRENT_USER);
		if (null == user) {
			return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getCODE(), "用户未登录!");
		}
		if (iUserService.checkAdminRole(user).isSuccess()) {
			// 查询当前节点的id以及递归子节点的id
			return iCategoryService.selectCategoryAndDeepChildrenByrenId(categoryId);
		}else {
			return ServerResponse.createByError("没有权限操作,需要管理员权限!");
		}
	}
	
	/**
	 * 查询全部节点
	 * @return
	 */
	@RequestMapping("listCategory.do")
	@ResponseBody
	public ServerResponse<List<Category>> listCategory(){
		ServerResponse<List<Category>> response = iCategoryService.listCategory();
		if (response.isSuccess()) {
			return response;
		}
		return ServerResponse.createByError();
	}
	
}
