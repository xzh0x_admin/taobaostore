package com.taobaostore.web.controller.backend;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.PageInfo;
import com.taobaostore.common.Const;
import com.taobaostore.common.ResponseCode;
import com.taobaostore.common.ServerResponse;
import com.taobaostore.pojo.User;
import com.taobaostore.service.IOrderService;
import com.taobaostore.service.IUserService;
import com.taobaostore.vo.OrderVo;

@Controller
@RequestMapping("/manage/order")
public class OrderManageController {
	
	@Autowired
	private IUserService iUserService;
	
	@Autowired
	private IOrderService iOrderService;
	
	
	
	/**
	 * 修改订单的状态
	 * @param session
	 * @param orderOn
	 * @return
	 */
	@RequestMapping("updateStatus.do")
	@ResponseBody
	public ServerResponse<String> manageOrderStatus(HttpSession session,Long orderOn,Integer statusCode){
		User user = (User)session.getAttribute(Const.CURRENT_USER);
		if (null == user) {
			return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getCODE(), "用户未登录!");
		}
		if (iUserService.checkAdminRole(user).isSuccess()) {
			// 是管理员 修改订单的状态
			return iOrderService.updateOrderStatus(user.getId(), orderOn, statusCode);
		}
		return null;
	}
	
	
	/**
	 * 查询订单列表
	 * @param session
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@RequestMapping("manageList.do")
	@ResponseBody
	public ServerResponse<PageInfo<OrderVo>> manageList(HttpSession session,@RequestParam(value = "pageNum",defaultValue = "1")Integer pageNum
			,@RequestParam(value = "pageSize",defaultValue = "10")Integer pageSize) {
		User user = (User)session.getAttribute(Const.CURRENT_USER);
		if (null == user) {
			return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getCODE(), "用户未登录!");
		}
		if (iUserService.checkAdminRole(user).isSuccess()) {
			// 是管理员 执行查询全部订单信息
			return iOrderService.manageList(pageNum, pageSize);
		}
		return null;
	}
}
