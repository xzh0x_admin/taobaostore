package com.taobaostore.web.controller.backend;

import java.math.BigDecimal;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageInfo;
import com.taobaostore.common.Const;
import com.taobaostore.common.ResponseCode;
import com.taobaostore.common.ServerResponse;
import com.taobaostore.pojo.Product;
import com.taobaostore.pojo.User;
import com.taobaostore.service.IProductService;
import com.taobaostore.service.IUserService;
import com.taobaostore.vo.ProductDetailVo;
import com.taobaostore.vo.ProductListVo;

@Controller
@RequestMapping("/manage/product")
public class ProductManageController {

	@Autowired
	private IUserService iUserService;
	@Autowired
	private IProductService iProductService;

	/**
	 * 新增商品
	 * @param session
	 * @param product
	 * @return
	 */
	@RequestMapping(value = "save_product.do")
	@ResponseBody
	public ServerResponse productSave(HttpSession session,Product product) {
		System.out.println(product);
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		if (user == null) {
			return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getCODE(),"用户未登录!");
		}
		if (iUserService.checkAdminRole(user).isSuccess()) {
			System.out.println("执行业务");
			// 执行业务方法
			return iProductService.saveOrUpdateProduct(product);
		}else {
			return ServerResponse.createByError("没有权限");
		}
	}
	
	/**
	 * 设置商品状态(上架,下架,删除)
	 * @param session
	 * @param productId
	 * @param status
	 * @return
	 */
	@RequestMapping("set_product_status.do")
	@ResponseBody
	public ServerResponse setProductStatus(HttpSession session,Integer productId,Integer status) {
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		if (user == null) {
			return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getCODE(),"用户未登录!");
		}
		if (iUserService.checkAdminRole(user).isSuccess()) {
			// 执行业务方法
			return iProductService.setProductStatus(productId, status);
		}else {
			return ServerResponse.createByError("没有权限");
		}
	}
	
	

	/**
	 * 分页返回商品列表
	 * @param session
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@RequestMapping("list.do")
	@ResponseBody
	public ServerResponse<PageInfo<ProductListVo>> listProduct(HttpSession session,@RequestParam(value = "pageNum",defaultValue = "1")Integer pageNum,
			@RequestParam(value = "pageSize",defaultValue = "10")Integer pageSize){
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		if (user == null) {
			return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getCODE(),"用户未登录!");
		}
		if (iUserService.checkAdminRole(user).isSuccess()) {
			// 执行业务方法
			return iProductService.listProduct(pageNum, pageSize);
		}
		return ServerResponse.createByError("没有权限");
	}



	/**
	 * 按照商品id筛选商品信息
	 * @param session
	 * @param productId
	 * @return
	 */
	@RequestMapping("listByProductId.do")
	@ResponseBody
	public ServerResponse<PageInfo<ProductListVo>> listProductByProductId(HttpSession session,@RequestParam(value = "pageNum",defaultValue = "1")Integer pageNum,
			@RequestParam(value = "pageSize",defaultValue = "10")Integer pageSize,Integer productId){
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		if (user == null) {
			return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getCODE(),"用户未登录!");
		}
		if (iUserService.checkAdminRole(user).isSuccess()) {
			// 执行业务方法
			return iProductService.getProductById(productId, pageNum, pageSize);
		}
		return ServerResponse.createByError("没有权限");
	}


	/**
	 * 根据商品名称模糊查询商品信息
	 * @param session
	 * @param productName
	 * @return
	 */
	@RequestMapping("listByProductName.do")
	@ResponseBody
	public ServerResponse<PageInfo<ProductListVo>> listProductByProductName(HttpSession session,@RequestParam(value = "pageNum",defaultValue = "1")Integer pageNum,
			@RequestParam(value = "pageSize",defaultValue = "10")Integer pageSize,String productName){
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		if (user == null) {
			return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getCODE(),"用户未登录!");
		}
		if (iUserService.checkAdminRole(user).isSuccess()) {
			// 执行业务方法
			return iProductService.listProductByName(productName, pageNum, pageSize);
		}
		return ServerResponse.createByError("没有权限");
	}
	
	/**
	 * 做编辑用户时跳转到指定页面
	 * @param session
	 * @param productId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@RequestMapping("updateToPage.do")
	@ResponseBody
	public ServerResponse<PageInfo<ProductDetailVo>> updateToPage(HttpSession session,Integer productId,@RequestParam(value = "pageNum",defaultValue = "1")Integer pageNum,
			@RequestParam(value = "pageSize",defaultValue = "10")Integer pageSize){
		User user = (User) session.getAttribute(Const.CURRENT_USER);
		if (user == null) {
			return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getCODE(),"用户未登录!");
		}
		if (iUserService.checkAdminRole(user).isSuccess()) {
			// 执行业务方法
			session.setAttribute(Const.PRODUCT_ID, productId);
			session.setAttribute(Const.PRODUCT_MSG, iProductService.getProductByIdDetail(productId, pageNum, pageSize).getData());
			return iProductService.getProductByIdDetail(productId, pageNum, pageSize);
		}
		return ServerResponse.createByError("没有权限");
	}

} 
