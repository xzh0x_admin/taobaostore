package com.taobaostore.web.controller.backend;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.taobaostore.common.Const;
import com.taobaostore.common.ServerResponse;
import com.taobaostore.pojo.User;
import com.taobaostore.service.IUserService;

@Controller
@RequestMapping("/manage/user/")
public class UserManageController {
	@Autowired
	private IUserService iUserService;
	
	@RequestMapping("login.do")
	@ResponseBody
	public ServerResponse<User> login(String username,String password,HttpSession session) {
		ServerResponse<User> response = iUserService.login(username, password);
		if (response.isSuccess()) {
			User user = response.getData();
			if (user.getRole() == Const.Role.ROLE_ADMIN) {
				// 说明登录的是管理员
				session.setAttribute(Const.CURRENT_USER,user);
				return ServerResponse.createBySuccess("登录成功!");
			}else {
				return ServerResponse.createByError("该账号不是管理员!");
			}
		}
		return ServerResponse.createByError("登录出现异常");
	}
}
