package com.taobaostore.web.controller.backend.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.taobaostore.common.Const;
import com.taobaostore.common.UserContext;

public class CheckLoginInterceptor extends HandlerInterceptorAdapter{
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		System.out.println("拦截请求backend");
		if (null == UserContext.getUser()) {
			response.sendRedirect("adminlogin.html");
			return false;
		}
		if (null != UserContext.getUser().getRole()) {
			if (UserContext.getUser().getRole() != Const.Role.ROLE_ADMIN) {
				response.sendRedirect("adminlogin.html");
				return false;
			}	
		}
		return true;
	}
}