package com.taobaostore.web.controller.portal;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.taobaostore.common.Const;
import com.taobaostore.common.ResponseCode;
import com.taobaostore.common.ServerResponse;
import com.taobaostore.pojo.User;
import com.taobaostore.service.ICartService;
import com.taobaostore.vo.CartVo;

@Controller
@RequestMapping("/cart/")
public class CartController {
	@Autowired
	private ICartService iCartService;
	
	/**
	 * 新增购物车记录(判断用户登录以及获取用户id信息,购买商品什么商品,数量??)
	 * @param session
	 * @param count
	 * @param productId
	 * @return
	 */
	@RequestMapping("add.do")
	@ResponseBody
    public ServerResponse<CartVo> add(HttpSession session,Integer count,Integer productId) {
    	User user = (User)session.getAttribute(Const.CURRENT_USER);
    	if (user == null) {
			return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getCODE(), ResponseCode.NEED_LOGIN.getDESC());
		}
		return iCartService.add(user.getId(), productId, count);
    }
	/**
	 * 删除购物车记录
	 * @param session
	 * @param count
	 * @param productId
	 * @return
	 */
	@RequestMapping("update.do")
	@ResponseBody
	public ServerResponse<CartVo> update(HttpSession session,Integer count,Integer productId) {
    	User user = (User)session.getAttribute(Const.CURRENT_USER);
    	if (user == null) {
    		return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getCODE(), ResponseCode.NEED_LOGIN.getDESC());
		}
		return iCartService.update(user.getId(), productId, count);
    }
	/**
	 * 删除购物车中的商品
	 * @param session
	 * @param productIds
	 * @return
	 */
	@RequestMapping("delete.do")
	@ResponseBody
	public ServerResponse<CartVo> deleteProduct(HttpSession session,String productIds) {
    	User user = (User)session.getAttribute(Const.CURRENT_USER);
    	if (user == null) {
    		return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getCODE(), ResponseCode.NEED_LOGIN.getDESC());
		}
		return iCartService.deleteProduct(user.getId(), productIds);
    }
	
	/**
	 * 显示当前用户下的所有购物车信息
	 * @param session
	 * @param userId
	 * @return
	 */
	@RequestMapping("list.do")
	@ResponseBody
	public ServerResponse<CartVo> list(HttpSession session,Integer userId){
		User user = (User)session.getAttribute(Const.CURRENT_USER);
    	if (user == null) {
    		return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getCODE(), ResponseCode.NEED_LOGIN.getDESC());
		}
    	return iCartService.list(userId);
	}
	
	/**
	 * 将商品全选或全反选
	 * @param session
	 * @param checkedStatus
	 * @return
	 */
	@RequestMapping("checkedAll.do")
	@ResponseBody
	public ServerResponse<CartVo> updateCheckedAll(HttpSession session,Integer checkedStatus){
		User user = (User)session.getAttribute(Const.CURRENT_USER);
    	if (user == null) {
    		return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getCODE(), ResponseCode.NEED_LOGIN.getDESC());
		}
    	if (null == checkedStatus) {
			return ServerResponse.createByError(ResponseCode.ILLEGAL_ARGUMENT.getCODE(), ResponseCode.ILLEGAL_ARGUMENT.getDESC());
		}else {
			// 等于1为全选,0为全反选
			if (checkedStatus == Const.Cart.CHECKED) {
				return iCartService.checkedAll(user.getId());
			}
			// 全反选
			if (checkedStatus == Const.Cart.ON_CHECKED) {
				return iCartService.UncheckedAll(user.getId());
			}
		}
    	return ServerResponse.createByError("未知的错误,请联系管理员!");
	}
	
	/**
	 * 选中单个商品或者反选单个商品
	 * @param session
	 * @param checkedStatus
	 * @param productId
	 * @return
	 */
	@RequestMapping("checked.do")
	@ResponseBody
	public ServerResponse<CartVo> updateUnCheckedAll(HttpSession session,Integer checkedStatus,Integer productId){
		System.out.println(checkedStatus);
		System.out.println(productId);
		User user = (User)session.getAttribute(Const.CURRENT_USER);
    	if (user == null) {
    		return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getCODE(), ResponseCode.NEED_LOGIN.getDESC());
		}
    	if (null == checkedStatus) {
			return ServerResponse.createByError(ResponseCode.ILLEGAL_ARGUMENT.getCODE(), ResponseCode.ILLEGAL_ARGUMENT.getDESC());
		}else {
			// 等于1为选中
			if (checkedStatus == Const.Cart.CHECKED) {
				System.out.println("改为选中");
				return iCartService.checkPitch(user.getId(), productId);
			}
			// 反选0(取消选中)
			if (checkedStatus == Const.Cart.ON_CHECKED) {
				System.out.println("改为反选");
				return iCartService.UncheckPitch(user.getId(), productId);
			}
		}
    	return ServerResponse.createByError("未知的错误,请联系管理员!");
	}
}
