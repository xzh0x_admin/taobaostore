package com.taobaostore.web.controller.portal;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.taobaostore.common.AliyunOSSUtil;
import com.taobaostore.common.ServerResponse;
import com.taobaostore.service.IFileService;

@Controller
public class FileController {
	@Autowired
	private IFileService fileService;
	
	@RequestMapping("/fileupload")
	@ResponseBody
    public String fileuploadMethod(MultipartFile pic,HttpServletRequest request) {
		String fileName = null;
		try {
//			response = fileService.upload(pic,request.getServletContext().getRealPath("upload"));
			fileName = AliyunOSSUtil.fileUpload(pic);
			if (fileName != null) {
				return fileName;
			}
		} catch (Exception e) {
			e.printStackTrace();
			return "上传失败";
		}
		return fileName;
	}
}
