package com.taobaostore.web.controller.portal;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;
import com.taobaostore.common.Const;
import com.taobaostore.common.ServerResponse;
import com.taobaostore.service.IProductService;
import com.taobaostore.vo.ProductDetailVo;

@Controller
@RequestMapping("/product/")
public class ProductController {
	@Autowired
    private IProductService iProductService;
	
	/**
	 * 点击查看商品详情
	 * @param productId
	 * @return
	 */
	@RequestMapping("detail.do")
	public String detail(HttpSession session,Integer productId){
		session.setAttribute(Const.FRONT_PRODUCT_MSG,iProductService.getProductDetail(productId));
		return "product_Msg";
	}
	
	/**
	 * 返回用户搜索的商品列表
	 * @param keyword
	 * @param categoryId
	 * @param pageNum
	 * @param pageSize
	 * @return
	 */
	@RequestMapping("list.do")
	@ResponseBody
	public ServerResponse<PageInfo> list(@RequestParam(value = "keyword",required = false)String keyword,
			@RequestParam(value = "categoryId",required = false)Integer categoryId,@RequestParam(value = "pageNum",defaultValue = "1")Integer pageNum,
			@RequestParam(value = "pageSize",defaultValue = "10")Integer pageSize){
				return iProductService.getProductByKeywordCategory(keyword, categoryId, pageNum, pageSize);
	}
	
	@RequestMapping("toSerachPage.do")
	public String toSerachPage(HttpSession session,String keyword) {
		session.setAttribute(Const.KEY_WORD, keyword);
		return "serachProduct";
	}
}
