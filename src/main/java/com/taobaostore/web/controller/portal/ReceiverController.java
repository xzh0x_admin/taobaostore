package com.taobaostore.web.controller.portal;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.github.pagehelper.PageInfo;
import com.taobaostore.common.Const;
import com.taobaostore.common.ResponseCode;
import com.taobaostore.common.ServerResponse;
import com.taobaostore.pojo.Receiver;
import com.taobaostore.pojo.User;
import com.taobaostore.service.IReceiverService;

@Controller
@RequestMapping("/receiver/")
public class ReceiverController {
	
	@Autowired
	private IReceiverService iReceiverService;

	/**
	 * 新增收货地址
	 * @param session
	 * @param receiver
	 * @return
	 */
	@RequestMapping("add.do")
	@ResponseBody
	public ServerResponse add(HttpSession session,Receiver receiver){
		User user = (User)session.getAttribute(Const.CURRENT_USER);
		if(user ==null){
			return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getCODE(),ResponseCode.NEED_LOGIN.getDESC());
		}
		receiver.setUserId(user.getId());
		return iReceiverService.add(receiver);
	}

	/**
	 * 显示全部收货地址
	 * @param pageNum
	 * @param pageSize
	 * @param session
	 * @return
	 */
	@RequestMapping("list.do")
	@ResponseBody
	public ServerResponse<PageInfo<Receiver>> list(@RequestParam(value = "pageNum",defaultValue = "1") int pageNum,
			@RequestParam(value = "pageSize",defaultValue = "10")int pageSize,
			HttpSession session){
		User user = (User)session.getAttribute(Const.CURRENT_USER);
		if(user == null){
			return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getCODE(),ResponseCode.NEED_LOGIN.getDESC());
		}
		return iReceiverService.list(pageNum, pageSize, user.getId());
	}
	
	/**
	 * 删除指定收货地址
	 * @param session
	 * @param receiverId
	 * @return
	 */
	@RequestMapping("delete.do")
	@ResponseBody
	public ServerResponse<String> delete(HttpSession session,Integer receiverId){
		User user = (User)session.getAttribute(Const.CURRENT_USER);
		if(user == null){
			return ServerResponse.createByError(ResponseCode.NEED_LOGIN.getCODE(),ResponseCode.NEED_LOGIN.getDESC());
		}
		return iReceiverService.delete(user.getId(),receiverId);
	}


}