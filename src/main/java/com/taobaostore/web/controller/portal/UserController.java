package com.taobaostore.web.controller.portal;

import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import javax.servlet.http.HttpSession;

import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import com.taobaostore.common.Const;
import com.taobaostore.common.ServerResponse;
import com.taobaostore.pojo.User;
import com.taobaostore.service.IUserService;
@Controller
@RequestMapping("/user/")
public class UserController {


	@Autowired
	private IUserService iuserService;


	/**
	 * 登录系统
	 * @param username
	 * @param password
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "login.do",method = RequestMethod.GET)
	@ResponseBody
	public ServerResponse<User> login(String username,String password,HttpSession session) {
		ServerResponse<User> response = iuserService.login(username, password);
		if (response.isSuccess()) {
			System.out.println(response.getData());
			session.setAttribute(Const.CURRENT_USER, response.getData());
			System.out.println("查询结果:"+response.getData());
		}
		return response;
	}


	/**
	 *  注销登录
	 * @param session
	 * @return
	 */
	@RequestMapping(value = "logout.do",method = RequestMethod.GET)
	@ResponseBody
	public ServerResponse<String> logout(HttpSession session){
		session.removeAttribute(Const.CURRENT_USER);
		return ServerResponse.createBySuccess();
	}


	/**
	 * 用户注册系统
	 * @return
	 */
	@RequestMapping(value = "register.do",method = RequestMethod.GET)
	@ResponseBody
	public ServerResponse<String> register(@Param("user")User user){
		return iuserService.register(user);
	}

	
	/**
	 * 获取当前用户信息
	 * @return
	 */
	@RequestMapping(value = "get_user_info.do",method = RequestMethod.GET)
	@ResponseBody
	public ServerResponse<User> getUserInfo(HttpSession session){
		User user = (User)session.getAttribute(Const.CURRENT_USER);
		if (user!=null) {
            return ServerResponse.createBySuccess(user);
		}
		return ServerResponse.createByError("用户未登录!");
	}
	
	
	/**
	 * 验证用户/密保是否存在
	 * @return
	 */
	@RequestMapping(value = "checkUserName.do",method = RequestMethod.GET)
	@ResponseBody
	public ServerResponse<String> checkUserName(String username){
		return iuserService.selectQuestion(username);
	}
	
	/**
	 * 进行验证密保答案
	 * @return
	 */
	@RequestMapping(value = "checkQuestion.do",method = RequestMethod.GET)
	@ResponseBody
	public ServerResponse<String> checkQuestion(String username,String question,String answer){
		return iuserService.checkQuestion(username, question, answer);
	}
	
	/**
	 * 忘记密码中的重置密码
	 * @param username
	 * @param passwordNew
	 * @param forgetToken
	 * @return
	 */
	@RequestMapping(value = "forgetResetPassword.do",method = RequestMethod.GET)
	@ResponseBody
	public ServerResponse<String> forgetResetPassword(String username,String passwordNew,String forgetToken){
		return iuserService.forgetResetPassword(username, passwordNew, forgetToken);
	}
	
	/**
	 *  更新用户信息
	 * @param user
	 * @return
	 */
	@RequestMapping(value = "updateUserMsg.do",method = RequestMethod.POST)
	public String updateUserMsg(User user,MultipartFile pic,HttpSession session){
		System.out.println("user信息:"+user);
		System.out.println("头像信息:"+pic.getOriginalFilename());
		ServerResponse<User> response = iuserService.updateUserMsg(user, pic);
		// 用户头像在session中带上动态目录
		if (response.isSuccess()) {
			User currentUser = response.getData();
			// 如果没有上传头像的话就将原本的头像目录上传
			if (null == currentUser.getHeadpic()) {
				currentUser.setHeadpic(getUserInfo(session).getData().getHeadpic());
			}
			session.setAttribute(Const.CURRENT_USER, currentUser);
		}
		return "redirect:/userMsg";
	}
	
	
	/**
	 * 在登录的情况下修改密码
	 * @return
	 */
	@RequestMapping(value = "resetPassword.do",method = RequestMethod.POST)
	public ServerResponse<String> resetPassword(HttpSession session,String passwordOld,String passwordNew){
		User user = (User)session.getAttribute(Const.CURRENT_USER);
		if (null == user) {
			return ServerResponse.createBySuccess("该用户还没有登录!,请返回登录!");
		}
		if (iuserService.resetPassword(passwordOld, passwordNew, user).isSuccess()) {
		    return ServerResponse.createBySuccess("修改密码成功!");	
		}
		return ServerResponse.createByError("修改密码失败!");
	}
}
 