package com.taobaostore.web.controller.portal.interceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import com.taobaostore.common.UserContext;

public class CheckLoginInterceptor extends HandlerInterceptorAdapter{
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		System.out.println("拦截请求");
		System.out.println(request.getServletPath());
		if (null == UserContext.getUser()) {
			response.sendRedirect("Land.html");
			return false;
		}
		return true;
	}
}