package com.taobaostore;

import static org.junit.Assert.*;

import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.taobaostore.dao.CartMapper;
import com.taobaostore.dao.OrderItemMapper;
import com.taobaostore.dao.ProductMapper;
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration("classpath:applicationContext.xml")
public class Test {
	private static Logger logger = LoggerFactory.getLogger(Test.class);
	@Autowired
	private OrderItemMapper OrderItemMapper;
	
	@Autowired
	private ProductMapper productMapper;
	
	@org.junit.Test
	public void testName() throws Exception {
        System.out.println(OrderItemMapper.listOrderItemByIdAndUserId(2, 123123123L));
	}
	
	@org.junit.Test
	public void test() throws Exception {
		System.out.println(productMapper.listProduct());
	}
	@Autowired
	private CartMapper cartMapper;
	@org.junit.Test
	public void listCartByUserIdAndChecked() throws Exception {
		System.out.println(cartMapper.listCartByUserIdAndChecked(2));
	}
	
	@org.junit.Test
	public void testCurrentSystem() throws Exception {
	   long currentTime = System.currentTimeMillis();
	   System.out.println(currentTime+currentTime%10);
	}
}