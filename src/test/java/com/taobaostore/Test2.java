package com.taobaostore;

import static org.junit.Assert.*;

import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Test2 {
	private static Logger logger = LoggerFactory.getLogger(Test2.class);
	@org.junit.Test
	public void testLog() throws Exception {
		logger.info("123");
		logger.error("321");
	}
}
