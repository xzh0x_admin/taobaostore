/*
Navicat MySQL Data Transfer

Source Server         : 本地连接
Source Server Version : 50523
Source Host           : localhost:3306
Source Database       : taobaosys

Target Server Type    : MYSQL
Target Server Version : 50523
File Encoding         : 65001

Date: 2020-01-20 16:21:00
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for tb_cart
-- ----------------------------
DROP TABLE IF EXISTS `tb_cart`;
CREATE TABLE `tb_cart` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '购物车id',
  `user_id` int(11) DEFAULT NULL COMMENT '所属用户',
  `product_id` int(11) DEFAULT NULL COMMENT '商品id',
  `quantity` int(11) DEFAULT NULL COMMENT '所选商品数量',
  `checked` int(11) DEFAULT NULL COMMENT '是否选择 1-已经选择 0-没有选中',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `user_id_index` (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_cart
-- ----------------------------
INSERT INTO `tb_cart` VALUES ('1', '2', '15', '1', '1', '2019-12-29 18:43:50', '2019-12-29 18:43:50');

-- ----------------------------
-- Table structure for tb_category
-- ----------------------------
DROP TABLE IF EXISTS `tb_category`;
CREATE TABLE `tb_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '类别id',
  `parent_id` int(11) DEFAULT NULL COMMENT '父类别 做递归',
  `name` varchar(255) DEFAULT NULL COMMENT '类别名称',
  `status` tinyint(1) DEFAULT NULL COMMENT '类别状态 1-正常 2-不可用',
  `sort_order` int(10) DEFAULT NULL COMMENT '排序编号,同一个梯队排序',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10014 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_category
-- ----------------------------
INSERT INTO `tb_category` VALUES ('10001', '0', '家用电器', '1', null, '2019-10-07 21:45:41', '2019-10-07 21:45:43');
INSERT INTO `tb_category` VALUES ('10002', '10001', '电脑', '1', null, '2019-10-07 21:45:41', '2019-10-07 21:45:43');
INSERT INTO `tb_category` VALUES ('10003', '10002', '华为电脑', '1', null, '2019-10-07 21:45:41', '2019-10-07 21:45:43');
INSERT INTO `tb_category` VALUES ('10004', '0', '小米手机', '1', null, '2019-10-07 21:45:41', '2019-10-07 21:45:43');
INSERT INTO `tb_category` VALUES ('10005', '0', '123', '1', null, '2019-10-25 13:09:52', '2019-10-25 13:09:52');
INSERT INTO `tb_category` VALUES ('10010', '0', '123', '1', null, '2019-10-25 13:10:20', '2019-10-25 13:10:20');
INSERT INTO `tb_category` VALUES ('10011', '0', '12523', '1', null, '2019-10-25 13:22:37', '2019-10-25 13:22:37');
INSERT INTO `tb_category` VALUES ('10012', '0', '123123', '1', null, '2019-10-25 13:32:22', '2019-10-25 13:32:22');
INSERT INTO `tb_category` VALUES ('10013', '0', '测试', '0', null, '2019-11-04 22:22:23', '2019-11-04 22:22:23');

-- ----------------------------
-- Table structure for tb_order
-- ----------------------------
DROP TABLE IF EXISTS `tb_order`;
CREATE TABLE `tb_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单id',
  `order_no` bigint(255) DEFAULT NULL COMMENT '订单号',
  `user_id` int(11) DEFAULT NULL COMMENT '下单的用户id',
  `receiver_id` int(11) DEFAULT NULL COMMENT '送货表id',
  `payment` decimal(20,2) DEFAULT NULL COMMENT '实际付款金额,单位是元,保留两位小数',
  `payment_type` int(255) DEFAULT NULL COMMENT '支付类型1-在线支付',
  `postage` int(255) DEFAULT NULL COMMENT '运费,单位是元',
  `status` int(255) DEFAULT NULL COMMENT '订单状态:0-取消,10-未付款,20-已付款,40-已发货,50-交易成功,60-交易关闭',
  `payment_time` datetime DEFAULT NULL COMMENT '支付时间',
  `send_time` datetime DEFAULT NULL COMMENT '发货时间',
  `end_time` datetime DEFAULT NULL COMMENT '交易完成时间',
  `close_time` datetime DEFAULT NULL COMMENT '交易关闭时间',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `user` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `order_on_index` (`order_no`) USING BTREE,
  UNIQUE KEY `user` (`user`),
  KEY `FKFA98EE3D2ECC6957` (`user`),
  CONSTRAINT `FKFA98EE3D2ECC6957` FOREIGN KEY (`user`) REFERENCES `tb_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_order
-- ----------------------------
INSERT INTO `tb_order` VALUES ('1', '123123123', '2', '1', '1000.00', '1', '0', '0', '2019-11-16 08:17:58', '2019-11-16 08:18:01', '2019-11-16 08:18:10', '2019-11-16 08:18:17', '2019-11-16 08:18:22', '2019-11-16 08:18:25', null);
INSERT INTO `tb_order` VALUES ('20', '1574309060192', '2', '9', '41000.00', '1', '0', '0', null, null, null, null, '2019-11-21 12:04:20', '2019-11-21 12:04:20', null);
INSERT INTO `tb_order` VALUES ('21', '1574405528252', '2', '9', '123.00', '1', '0', '0', null, null, null, null, '2019-11-22 14:52:08', '2019-11-22 14:52:08', null);
INSERT INTO `tb_order` VALUES ('22', '1574411394000', '2', '9', '1000.00', '1', '0', '0', '2019-11-22 16:30:17', null, null, null, '2019-11-22 16:29:53', '2019-11-22 16:29:53', null);
INSERT INTO `tb_order` VALUES ('23', '1574417760676', '2', '14', '1000.00', '1', '0', '0', null, null, null, null, '2019-11-22 18:16:00', '2019-11-22 18:16:00', null);
INSERT INTO `tb_order` VALUES ('24', '1574469127554', '2', '14', '1000.00', '1', '0', '0', null, null, null, null, '2019-11-23 08:32:07', '2019-11-23 08:32:07', null);
INSERT INTO `tb_order` VALUES ('25', '1574482583224', '8', '1', '200000.00', '1', '0', '0', null, null, null, null, '2019-11-23 12:16:23', '2019-11-23 12:16:23', null);
INSERT INTO `tb_order` VALUES ('26', '1574482679836', '8', '16', '1230.00', '1', '0', '0', null, null, null, null, '2019-11-23 12:17:59', '2019-11-23 12:17:59', null);
INSERT INTO `tb_order` VALUES ('27', '1574482873328', '8', '16', '1000.00', '1', '0', '10', null, null, null, null, '2019-11-23 12:21:13', '2019-11-23 12:21:13', null);
INSERT INTO `tb_order` VALUES ('28', '1574482922186', '2', '1', '20000.00', '1', '0', '0', null, null, null, null, '2019-11-23 12:22:02', '2019-11-23 12:22:02', null);
INSERT INTO `tb_order` VALUES ('29', '1574482982386', '19', '17', '1000.00', '1', '0', '10', null, null, null, null, '2019-11-23 12:23:02', '2019-11-23 12:23:02', null);
INSERT INTO `tb_order` VALUES ('30', '1574483186316', '20', '18', '60000.00', '1', '0', '0', '2019-11-23 12:27:39', null, null, null, '2019-11-23 12:26:26', '2019-11-23 12:26:26', null);
INSERT INTO `tb_order` VALUES ('31', '1574572909286', '21', '19', '1000.00', '1', '0', '10', null, null, null, null, '2019-11-24 13:21:49', '2019-11-24 13:21:49', null);
INSERT INTO `tb_order` VALUES ('32', '1574573377006', '2', '9', '21000.00', '1', '0', '0', null, null, null, null, '2019-11-24 13:29:36', '2019-11-24 13:29:36', null);
INSERT INTO `tb_order` VALUES ('33', '1574577926698', '2', '14', '20000.00', '1', '0', '0', null, null, null, null, '2019-11-24 14:45:26', '2019-11-24 14:45:26', null);
INSERT INTO `tb_order` VALUES ('34', '1574583670114', '2', '1', '1000.00', '1', '0', '10', null, null, null, null, '2019-11-24 16:21:10', '2019-11-24 16:21:10', null);
INSERT INTO `tb_order` VALUES ('35', '1574583725564', '2', '14', '20000.00', '1', '0', '10', null, null, null, null, '2019-11-24 16:22:05', '2019-11-24 16:22:05', null);
INSERT INTO `tb_order` VALUES ('36', '1574583804190', '21', '19', '20000.00', '1', '0', '10', null, null, null, null, '2019-11-24 16:23:24', '2019-11-24 16:23:24', null);
INSERT INTO `tb_order` VALUES ('37', '1574605235838', '2', '9', '20000.00', '1', '0', '10', null, null, null, null, '2019-11-24 22:20:35', '2019-11-24 22:20:35', null);
INSERT INTO `tb_order` VALUES ('38', '1574769519904', '2', '9', '20000.00', '1', '0', '10', null, null, null, null, '2019-11-26 19:58:39', '2019-11-26 19:58:39', null);
INSERT INTO `tb_order` VALUES ('39', '1574769519907', '2', '9', '20000.00', '1', '0', '10', '2019-12-01 16:43:12', '2019-12-01 16:43:18', '2019-12-01 16:43:20', '2019-12-01 16:43:21', '2019-11-26 19:58:39', '2019-11-26 19:58:39', null);

-- ----------------------------
-- Table structure for tb_order_item
-- ----------------------------
DROP TABLE IF EXISTS `tb_order_item`;
CREATE TABLE `tb_order_item` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '订单子表(订单详细)id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `order_no` bigint(20) DEFAULT NULL COMMENT '订单编号',
  `product_id` int(11) DEFAULT NULL COMMENT '商品id',
  `product_name` varchar(255) DEFAULT NULL COMMENT '商品名称',
  `product_image` varchar(255) DEFAULT NULL COMMENT '商品图片',
  `current_unit_price` decimal(20,2) DEFAULT NULL COMMENT '商品单价',
  `quantity` int(255) DEFAULT NULL COMMENT '商品数量',
  `total_price` decimal(20,2) DEFAULT NULL COMMENT '商品总价格',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `order_no_user_id_index` (`user_id`,`order_no`) USING BTREE,
  KEY `order_no_index` (`order_no`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_order_item
-- ----------------------------
INSERT INTO `tb_order_item` VALUES ('1', '2', '123123123', '1', '手机测试', 'http://49.232.168.42/963a1120-2681-44d1-878f-9bbf5271a736.jpg', '1000.01', '1', '1000.00', '2019-11-16 08:26:32', '2019-11-16 08:26:35');
INSERT INTO `tb_order_item` VALUES ('11', '2', '1574309060192', '15', '小米MIX阿尔法', 'http://49.232.168.42/963a1120-2681-44d1-878f-9bbf5271a736.jpg', '20000.00', '2', '40000.00', '2019-11-21 12:04:20', '2019-11-21 12:04:20');
INSERT INTO `tb_order_item` VALUES ('12', '2', '1574309060192', '16', '小米5', 'http://49.232.168.42/83bb0ef6-c0a7-4634-9cf9-efb436df8fbe.jpg', '1000.00', '1', '1000.00', '2019-11-21 12:04:20', '2019-11-21 12:04:20');
INSERT INTO `tb_order_item` VALUES ('13', '2', '1574405528252', '22', '小米ssjsj', 'http://49.232.168.42/f123fabe-e6e9-4080-9e58-a2432875b437.jpg', '123.00', '1', '123.00', '2019-11-22 14:52:08', '2019-11-22 14:52:08');
INSERT INTO `tb_order_item` VALUES ('14', '2', '1574411394000', '26', '邱邱', 'http://49.232.168.42/387696f0-b743-45e1-8cee-4a779b3184eb.jpg', '1000.00', '1', '1000.00', '2019-11-22 16:29:54', '2019-11-22 16:29:54');
INSERT INTO `tb_order_item` VALUES ('15', '2', '1574417760676', '29', '小黑記事本', 'http://49.232.168.42/f7c27fc0-33a8-466f-bd52-d15e31fb205c.jpg', '1000.00', '1', '1000.00', '2019-11-22 18:16:00', '2019-11-22 18:16:00');
INSERT INTO `tb_order_item` VALUES ('16', '2', '1574469127554', '16', '小米5', 'http://49.232.168.42/83bb0ef6-c0a7-4634-9cf9-efb436df8fbe.jpg', '1000.00', '1', '1000.00', '2019-11-23 08:32:07', '2019-11-23 08:32:07');
INSERT INTO `tb_order_item` VALUES ('17', '8', '1574482583224', '15', '小米MIX阿尔法', 'http://49.232.168.42/963a1120-2681-44d1-878f-9bbf5271a736.jpg', '20000.00', '10', '200000.00', '2019-11-23 12:16:23', '2019-11-23 12:16:23');
INSERT INTO `tb_order_item` VALUES ('18', '8', '1574482679836', '22', '小米ssjsj', 'http://49.232.168.42/f123fabe-e6e9-4080-9e58-a2432875b437.jpg', '123.00', '10', '1230.00', '2019-11-23 12:17:59', '2019-11-23 12:17:59');
INSERT INTO `tb_order_item` VALUES ('19', '8', '1574482873328', '16', '小米5', 'http://49.232.168.42/83bb0ef6-c0a7-4634-9cf9-efb436df8fbe.jpg', '1000.00', '1', '1000.00', '2019-11-23 12:21:13', '2019-11-23 12:21:13');
INSERT INTO `tb_order_item` VALUES ('20', '2', '1574482922186', '15', '小米MIX阿尔法', 'http://49.232.168.42/963a1120-2681-44d1-878f-9bbf5271a736.jpg', '20000.00', '1', '20000.00', '2019-11-23 12:22:02', '2019-11-23 12:22:02');
INSERT INTO `tb_order_item` VALUES ('21', '19', '1574482982386', '16', '小米5', 'http://49.232.168.42/83bb0ef6-c0a7-4634-9cf9-efb436df8fbe.jpg', '1000.00', '1', '1000.00', '2019-11-23 12:23:02', '2019-11-23 12:23:02');
INSERT INTO `tb_order_item` VALUES ('22', '20', '1574483186316', '15', '小米MIX阿尔法', 'http://49.232.168.42/963a1120-2681-44d1-878f-9bbf5271a736.jpg', '20000.00', '3', '60000.00', '2019-11-23 12:26:26', '2019-11-23 12:26:26');
INSERT INTO `tb_order_item` VALUES ('23', '21', '1574572909286', '16', '小米5', 'http://49.232.168.42/83bb0ef6-c0a7-4634-9cf9-efb436df8fbe.jpg', '1000.00', '1', '1000.00', '2019-11-24 13:21:49', '2019-11-24 13:21:49');
INSERT INTO `tb_order_item` VALUES ('24', '2', '1574573377006', '15', '小米MIX阿尔法', 'http://49.232.168.42/963a1120-2681-44d1-878f-9bbf5271a736.jpg', '20000.00', '1', '20000.00', '2019-11-24 13:29:37', '2019-11-24 13:29:37');
INSERT INTO `tb_order_item` VALUES ('25', '2', '1574573377006', '16', '小米5', 'http://49.232.168.42/83bb0ef6-c0a7-4634-9cf9-efb436df8fbe.jpg', '1000.00', '1', '1000.00', '2019-11-24 13:29:37', '2019-11-24 13:29:37');
INSERT INTO `tb_order_item` VALUES ('26', '2', '1574577926698', '15', '小米MIX阿尔法', 'http://49.232.168.42/963a1120-2681-44d1-878f-9bbf5271a736.jpg', '20000.00', '1', '20000.00', '2019-11-24 14:45:26', '2019-11-24 14:45:26');
INSERT INTO `tb_order_item` VALUES ('27', '2', '1574583670114', '16', '小米5', 'http://49.232.168.42/83bb0ef6-c0a7-4634-9cf9-efb436df8fbe.jpg', '1000.00', '1', '1000.00', '2019-11-24 16:21:10', '2019-11-24 16:21:10');
INSERT INTO `tb_order_item` VALUES ('28', '2', '1574583725564', '15', '小米MIX阿尔法', 'http://49.232.168.42/963a1120-2681-44d1-878f-9bbf5271a736.jpg', '20000.00', '1', '20000.00', '2019-11-24 16:22:05', '2019-11-24 16:22:05');
INSERT INTO `tb_order_item` VALUES ('29', '21', '1574583804190', '15', '小米MIX阿尔法', 'http://49.232.168.42/963a1120-2681-44d1-878f-9bbf5271a736.jpg', '20000.00', '1', '20000.00', '2019-11-24 16:23:24', '2019-11-24 16:23:24');
INSERT INTO `tb_order_item` VALUES ('30', '2', '1574605235838', '15', '小米MIX阿尔法', 'http://49.232.168.42/963a1120-2681-44d1-878f-9bbf5271a736.jpg', '20000.00', '1', '20000.00', '2019-11-24 22:20:35', '2019-11-24 22:20:35');
INSERT INTO `tb_order_item` VALUES ('31', '2', '1574769519904', '15', '小米MIX阿尔法', 'http://49.232.168.42/963a1120-2681-44d1-878f-9bbf5271a736.jpg', '20000.00', '1', '20000.00', '2019-11-26 19:58:39', '2019-11-26 19:58:39');

-- ----------------------------
-- Table structure for tb_pay_info
-- ----------------------------
DROP TABLE IF EXISTS `tb_pay_info`;
CREATE TABLE `tb_pay_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) DEFAULT NULL COMMENT '哪个用户支付的',
  `order_no` bigint(20) DEFAULT NULL COMMENT '订单号',
  `pay_platform` int(10) DEFAULT NULL COMMENT '支付平台:1-支付宝,2-微信',
  `platform_number` varchar(255) DEFAULT NULL COMMENT '支付流水号',
  `platform_status` varchar(20) DEFAULT NULL COMMENT '支付状态',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_pay_info
-- ----------------------------
INSERT INTO `tb_pay_info` VALUES ('1', '2', '312123123', '1', '2019111922001493691000060477', 'WAIT_BUYER_PAY', '2019-11-19 20:40:02', '2019-11-19 20:40:07');
INSERT INTO `tb_pay_info` VALUES ('2', '2', '312123123', '1', '2019111922001493691000060477', 'TRADE_SUCCESS', '2019-11-19 20:40:05', '2019-11-19 20:40:09');
INSERT INTO `tb_pay_info` VALUES ('3', '2', '1574411394000', '1', '2019112222001493691000070255', 'WAIT_BUYER_PAY', '2019-11-22 16:30:13', '2019-11-22 16:30:13');
INSERT INTO `tb_pay_info` VALUES ('4', '2', '1574411394000', '1', '2019112222001493691000070255', 'TRADE_SUCCESS', '2019-11-22 16:30:18', '2019-11-22 16:30:18');
INSERT INTO `tb_pay_info` VALUES ('5', '20', '1574483186316', '1', '2019112322001493691000069179', 'WAIT_BUYER_PAY', '2019-11-23 12:27:15', '2019-11-23 12:27:15');
INSERT INTO `tb_pay_info` VALUES ('6', '20', '1574483186316', '1', '2019112322001493691000069179', 'TRADE_SUCCESS', '2019-11-23 12:27:40', '2019-11-23 12:27:40');

-- ----------------------------
-- Table structure for tb_product
-- ----------------------------
DROP TABLE IF EXISTS `tb_product`;
CREATE TABLE `tb_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '商品id',
  `category_id` int(11) DEFAULT NULL COMMENT '分类id',
  `name` varchar(255) NOT NULL COMMENT '商品名称',
  `subtitle` varchar(255) NOT NULL COMMENT '商品副标题',
  `main_image` varchar(255) DEFAULT NULL COMMENT '产品主图 url相对路径',
  `sub_images` varchar(255) DEFAULT NULL COMMENT '图片地址,json格式,拓展用',
  `detail` text COMMENT '商品详情',
  `price` decimal(20,2) NOT NULL COMMENT '商品单价',
  `stock` varchar(255) NOT NULL COMMENT '库存',
  `status` varchar(255) DEFAULT NULL COMMENT '商品状态1-在售,2-下架,3-删除',
  `create_time` datetime DEFAULT NULL,
  `update_time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_product
-- ----------------------------
INSERT INTO `tb_product` VALUES ('1', '10001', 'iphone手机', '性能强', '123123', '123123', '123123', '1123123.00', '123', '1', '2019-10-07 21:46:49', '2019-10-07 21:46:52');
INSERT INTO `tb_product` VALUES ('2', '10001', '台式笔记本电脑', '联想笔记本电脑 i7处理器 固态硬盘', 'xxxxx', 'xxxxxx', '联想笔记本电脑 i7处理器 固态硬盘', '2000.00', '10', '2', '2019-10-07 21:46:49', '2019-10-07 21:46:52');
INSERT INTO `tb_product` VALUES ('10', '10001', '华为笔记本', '这个笔记本的性能很强', null, null, 'i7处理器 全固态阵列', '10000.00', '100', '2', '2019-10-20 13:11:33', '2019-10-20 13:11:33');
INSERT INTO `tb_product` VALUES ('11', '10001', '小米MIX3', '骁龙845', '', '', 'i7处理器 全固态阵列', '10000.00', '100', '1', '2019-10-20 13:11:33', '2019-10-20 13:11:33');
INSERT INTO `tb_product` VALUES ('12', '10001', '小米MIX4', '骁龙845', '', '', 'i7处理器 全固态阵列', '10000.00', '100', '1', '2019-10-20 13:11:33', '2019-10-20 13:11:33');
INSERT INTO `tb_product` VALUES ('13', '10001', '小米MIX5', '骁龙845', 'http://49.232.168.42/a901ffdd-d177-4377-8527-ebf6baff035b.jpg', 'http://49.232.168.42/a901ffdd-d177-4377-8527-ebf6baff035b.jpg,http://49.232.168.42/105f334b-7204-4f87-b881-2b9b05326210.jpg,', '<h1><span style=\"color: rgb(206, 231, 247); background-color: rgb(99, 99, 99);\">小米MIX5 性能&nbsp; &nbsp; &nbsp;</span></h1><p><img src=\"http://49.232.168.42/f8e78d5d-5c57-441d-a0c9-ef365e40a643.jpg\" style=\"width: 25%;\"></p><p>我们得这个挺不错的</p><p><br></p>', '11.00', '10', '2', '2019-10-25 00:12:50', '2019-10-25 00:12:50');
INSERT INTO `tb_product` VALUES ('14', '10001', '小米6', '骁龙845', null, '', '<p><br></p>', '11.00', '10', '1', '2019-10-25 00:15:41', '2019-10-25 00:15:41');
INSERT INTO `tb_product` VALUES ('15', '10001', '小米MIX阿尔法', '性能极强,跨世纪手机', 'http://49.232.168.42/963a1120-2681-44d1-878f-9bbf5271a736.jpg', 'http://49.232.168.42/963a1120-2681-44d1-878f-9bbf5271a736.jpg,', '<p><img src=\"http://49.232.168.42/963a1120-2681-44d1-878f-9bbf5271a736.jpg\" style=\"width: 25%;\">我是赵帅asd1asdasd<img src=\"http://49.232.168.42/1a55d8e0-6cef-4c12-b3ed-34304e0cc6f0.jpg\" style=\"width: 778px;\"><br></p>', '20000.00', '75', '1', '2019-10-25 00:18:17', '2019-10-25 00:18:17');
INSERT INTO `tb_product` VALUES ('16', '10002', '小米5', '骁龙820', 'http://49.232.168.42/83bb0ef6-c0a7-4634-9cf9-efb436df8fbe.jpg', 'http://49.232.168.42/83bb0ef6-c0a7-4634-9cf9-efb436df8fbe.jpg,', '<p><img src=\"http://49.232.168.42/83bb0ef6-c0a7-4634-9cf9-efb436df8fbe.jpg\" style=\"width: 25%;\">在<br></p>', '1000.00', '91', '1', '2019-10-25 00:34:19', '2019-10-25 00:34:19');
INSERT INTO `tb_product` VALUES ('17', '10001', 'oppo手机', 'oppo拍照好', 'http://49.232.168.42/f1ccf108-c82f-4fdc-a40e-352efcf3a1e8.jpg', 'http://49.232.168.42/f1ccf108-c82f-4fdc-a40e-352efcf3a1e8.jpg,', '<p><img src=\"http://49.232.168.42/f1ccf108-c82f-4fdc-a40e-352efcf3a1e8.jpg\" style=\"width: 25%;\">123<br></p>', '1000.00', '1', '1', '2019-10-25 00:35:57', '2019-10-25 00:35:57');
INSERT INTO `tb_product` VALUES ('18', '10002', 'oppo手机', 'oppo拍照好', 'http://49.232.168.42/f1ccf108-c82f-4fdc-a40e-352efcf3a1e8.jpg', 'http://49.232.168.42/f1ccf108-c82f-4fdc-a40e-352efcf3a1e8.jpg,', '<p><img src=\"http://49.232.168.42/f1ccf108-c82f-4fdc-a40e-352efcf3a1e8.jpg\" style=\"width: 25%;\">123<br></p>', '1000.00', '1', '1', '2019-10-25 00:36:16', '2019-10-25 00:36:16');
INSERT INTO `tb_product` VALUES ('19', '10002', 'oppo手机', 'oppo拍照好', 'http://49.232.168.42/f1ccf108-c82f-4fdc-a40e-352efcf3a1e8.jpg', 'http://49.232.168.42/f1ccf108-c82f-4fdc-a40e-352efcf3a1e8.jpg,http://49.232.168.42/795d6bfb-0149-4d71-b11e-1edb07c649c7.jpg,', '<p><img src=\"http://49.232.168.42/f1ccf108-c82f-4fdc-a40e-352efcf3a1e8.jpg\" style=\"width: 25%;\">123<img src=\"http://49.232.168.42/795d6bfb-0149-4d71-b11e-1edb07c649c7.jpg\" style=\"width: 778px;\"><br></p>', '1000.00', '1', '1', '2019-10-25 00:36:43', '2019-10-25 00:36:43');
INSERT INTO `tb_product` VALUES ('20', '10002', '搜搜搜', '123123', 'http://49.232.168.42/13333e1c-f454-47df-ac4f-5365f14fdb1d.jpg', 'http://49.232.168.42/13333e1c-f454-47df-ac4f-5365f14fdb1d.jpg,http://49.232.168.42/b1609b67-2232-4fe3-ac9e-d26e6e5269c4.jpg,', '<p><img src=\"http://49.232.168.42/13333e1c-f454-47df-ac4f-5365f14fdb1d.jpg\" style=\"width: 25%;\">123<img src=\"http://49.232.168.42/b1609b67-2232-4fe3-ac9e-d26e6e5269c4.jpg\" style=\"width: 778px;\"><br></p>', '123.00', '1', '1', '2019-10-25 00:41:08', '2019-10-25 00:41:08');
INSERT INTO `tb_product` VALUES ('21', '10002', 'vivo1', 'vivo1', 'http://49.232.168.42/5055bed1-7ab0-4eae-944e-ac4bd16b5188.jpg', 'http://49.232.168.42/5055bed1-7ab0-4eae-944e-ac4bd16b5188.jpg,http://49.232.168.42/7bb3e4f7-5bd4-4b7b-a12a-4aae3981d57b.jpg,', '<p><img src=\"http://49.232.168.42/5055bed1-7ab0-4eae-944e-ac4bd16b5188.jpg\" style=\"width: 778px;\"><img src=\"http://49.232.168.42/7bb3e4f7-5bd4-4b7b-a12a-4aae3981d57b.jpg\" style=\"width: 750px;\"><br></p>', '1.00', '1', '1', '2019-10-25 00:42:36', '2019-10-25 00:42:36');
INSERT INTO `tb_product` VALUES ('22', '10004', '小米ssjsj', '123123', 'http://49.232.168.42/f123fabe-e6e9-4080-9e58-a2432875b437.jpg', 'http://49.232.168.42/f123fabe-e6e9-4080-9e58-a2432875b437.jpg,http://49.232.168.42/fc0ea9a8-6672-4397-ae06-596888f18d74.jpg,', '<p><img src=\"http://49.232.168.42/f123fabe-e6e9-4080-9e58-a2432875b437.jpg\" style=\"width: 25%;\"><img src=\"http://49.232.168.42/fc0ea9a8-6672-4397-ae06-596888f18d74.jpg\" style=\"width: 25%;\"><br></p>', '123.00', '89', '1', '2019-10-25 00:51:29', '2019-10-25 00:51:29');
INSERT INTO `tb_product` VALUES ('23', '10001', '华为meta20', '麒麟970', 'http://49.232.168.42/5264d17f-4dce-4497-891f-8f5d6e3e2dbc.jpg', 'http://49.232.168.42/5264d17f-4dce-4497-891f-8f5d6e3e2dbc.jpg,', '<p><img src=\"http://49.232.168.42/5264d17f-4dce-4497-891f-8f5d6e3e2dbc.jpg\" style=\"width: 25%;\"><br></p>', '3000.00', '1', '1', '2019-10-25 10:53:31', '2019-10-25 10:53:31');
INSERT INTO `tb_product` VALUES ('24', '10001', '三星note6', '可以用来赠送亲朋好友,来卖', null, '', '<p><span style=\"font-family: &quot;Comic Sans MS&quot;; background-color: rgb(206, 222, 231); color: rgb(74, 123, 140);\">你要是想坑人的话当然可以选用我们的三星note6</span></p><ul><li><span style=\"font-family: &quot;Times New Roman&quot;; background-color: rgb(99, 99, 99); color: rgb(255, 255, 0);\">性能强劲</span></li></ul>', '123123.00', '12', '1', '2019-10-25 22:34:39', '2019-10-25 22:34:39');
INSERT INTO `tb_product` VALUES ('25', '10001', '邱实特卖', '邱实能打扫卫生能洗衣做饭什么都能干还能洗厕所', 'http://49.232.168.42/100545a9-cdb8-4ec8-8631-7c72e97b683b.jpg', 'http://49.232.168.42/100545a9-cdb8-4ec8-8631-7c72e97b683b.jpg,', '<h2><span style=\"background-color: rgb(206, 198, 206); color: rgb(255, 239, 198);\">社会我丘石 人很屁话多</span></h2><h2><img src=\"http://49.232.168.42/100545a9-cdb8-4ec8-8631-7c72e97b683b.jpg\" style=\"width: 25%;\"><span style=\"background-color: rgb(206, 198, 206); color: rgb(255, 239, 198);\"><br></span></h2><ol><li><span style=\"background-color: rgb(206, 198, 206); color: rgb(255, 239, 198);\">丘石特性:</span></li><li><span style=\"color: rgb(255, 239, 198); background-color: rgb(206, 198, 206);\">打扫卫生</span></li><li><span style=\"color: rgb(255, 239, 198); background-color: rgb(206, 198, 206);\">洗衣做饭</span></li><li><span style=\"color: rgb(255, 239, 198); background-color: rgb(206, 198, 206);\">买东西</span></li><li><span style=\"color: rgb(255, 239, 198); background-color: rgb(206, 198, 206);\">睡觉</span></li><li><span style=\"color: rgb(255, 239, 198); background-color: rgb(206, 198, 206);\">打游戏</span></li><li><span style=\"color: rgb(255, 239, 198); background-color: rgb(206, 198, 206);\">喜欢英雄联盟 喜欢游戏</span></li><li><span style=\"color: rgb(255, 239, 198); background-color: rgb(206, 198, 206);\">格言:我丘石就是喜欢玩</span></li></ol><table class=\"table table-bordered\"><tbody><tr><td>邱实信息表</td><td>年龄</td><td>姓名</td><td>地址</td><td>手机号</td></tr><tr><td>爱玩</td><td>20</td><td>邱实</td><td>北京</td><td>123123123</td></tr></tbody></table><ol><li><span style=\"color: rgb(255, 239, 198); background-color: rgb(206, 198, 206);\"><br></span></li></ol><h2><span style=\"background-color: rgb(206, 198, 206); color: rgb(255, 239, 198);\"><br></span></h2>', '0.01', '10000', '2', '2019-10-26 08:42:01', '2019-10-26 08:42:01');
INSERT INTO `tb_product` VALUES ('26', '10001', '邱邱', '邱邱', 'http://49.232.168.42/387696f0-b743-45e1-8cee-4a779b3184eb.jpg', 'http://49.232.168.42/387696f0-b743-45e1-8cee-4a779b3184eb.jpg,http://49.232.168.42/3a78994a-9dc2-4204-8c21-aff66075de22.jpg,', '<p><img src=\"http://49.232.168.42/3a78994a-9dc2-4204-8c21-aff66075de22.jpg\" style=\"width: 100%;\"></p><h1>性能强大</h1>', '1000.00', '0', '1', '2019-10-30 10:13:48', '2019-10-30 10:13:48');
INSERT INTO `tb_product` VALUES ('27', '10001', '华为meta20pro', '华为meta20pro', 'http://49.232.168.42/e83b327a-6bf8-441e-ac79-47806977866f.jpg', 'http://49.232.168.42/e83b327a-6bf8-441e-ac79-47806977866f.jpg,', '<h1><span style=\"background-color: rgb(0, 255, 0); color: rgb(156, 156, 148);\">华为meta20pro</span></h1><h1><img src=\"http://49.232.168.42/e83b327a-6bf8-441e-ac79-47806977866f.jpg\" style=\"width: 25%;\"><span style=\"background-color: rgb(0, 255, 0); color: rgb(156, 156, 148);\"><br></span></h1>', '3000.00', '1', '1', '2019-11-01 22:14:38', '2019-11-01 22:14:38');
INSERT INTO `tb_product` VALUES ('28', '10001', 'aaa', 'aaa', 'http://49.232.168.42/161677f0-98c4-4b26-b07a-07e017a14faa.jpg', 'http://49.232.168.42/161677f0-98c4-4b26-b07a-07e017a14faa.jpg,', '<p><img src=\"http://49.232.168.42/161677f0-98c4-4b26-b07a-07e017a14faa.jpg\" style=\"width: 25%;\"></p>', '11.00', '11', '1', '2019-11-05 21:44:21', '2019-11-05 21:44:21');
INSERT INTO `tb_product` VALUES ('29', '10001', '小黑記事本', '小黑記事本', 'http://49.232.168.42/f7c27fc0-33a8-466f-bd52-d15e31fb205c.jpg', 'http://49.232.168.42/f7c27fc0-33a8-466f-bd52-d15e31fb205c.jpg,', '<p>小黑即使本<img src=\"http://49.232.168.42/f7c27fc0-33a8-466f-bd52-d15e31fb205c.jpg\" style=\"width: 25%;\"></p>', '1000.00', '0', '1', '2019-11-22 18:15:13', '2019-11-22 18:15:13');

-- ----------------------------
-- Table structure for tb_receiver
-- ----------------------------
DROP TABLE IF EXISTS `tb_receiver`;
CREATE TABLE `tb_receiver` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '地址表id',
  `user_id` int(11) DEFAULT NULL COMMENT '用户id',
  `receiver_name` varchar(255) DEFAULT NULL COMMENT '收货姓名',
  `recelver_phone` varchar(255) DEFAULT NULL COMMENT '收货固定电话',
  `receiver_mobile` varchar(255) DEFAULT NULL COMMENT '收货移动电话',
  `receiver_province` varchar(255) DEFAULT NULL COMMENT '省份',
  `receiver_city` varchar(255) DEFAULT NULL COMMENT '城市',
  `receiver_district` varchar(255) DEFAULT NULL COMMENT '区/县',
  `receiver_address` varchar(255) DEFAULT NULL COMMENT '详细地址',
  `receiver_zip` varchar(255) DEFAULT NULL COMMENT '邮编',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_receiver
-- ----------------------------
INSERT INTO `tb_receiver` VALUES ('1', '2', '孟成', '17759071514', '810003919', '北京', '北京', '昌平', '地址', '124111', '2019-11-02 09:41:29', '2019-11-02 09:41:31');
INSERT INTO `tb_receiver` VALUES ('9', '2', '薛梓芃', '123123', '123123', '福建省', '福州', null, '福清市龙田镇111', '123123', '2019-11-08 15:52:13', '2019-11-08 15:52:13');
INSERT INTO `tb_receiver` VALUES ('14', '2', '吴芷杰', '17347456473', '17347456473', '湖南省', '怀化', null, '芷江', '123123', '2019-11-12 11:24:00', '2019-11-12 11:24:00');
INSERT INTO `tb_receiver` VALUES ('16', '8', '吴芷杰', '17347456473', '17347456473', '湖南省', '怀化', null, 'xzpiloveu', '999999', '2019-11-23 12:17:02', '2019-11-23 12:17:02');
INSERT INTO `tb_receiver` VALUES ('17', '19', '123', '123', '123', '北京', '北京', null, '123', '123', '2019-11-23 12:22:59', '2019-11-23 12:22:59');
INSERT INTO `tb_receiver` VALUES ('18', '20', '吴芷杰', '17347456473', '17347456473', '湖南省', '怀化', null, 'xzp', '999999', '2019-11-23 12:26:17', '2019-11-23 12:26:17');
INSERT INTO `tb_receiver` VALUES ('19', '21', '成耶', '123123', '123123', '上海', '上海', null, '123123', '123123', '2019-11-24 13:21:46', '2019-11-24 13:21:46');

-- ----------------------------
-- Table structure for tb_user
-- ----------------------------
DROP TABLE IF EXISTS `tb_user`;
CREATE TABLE `tb_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '用户表id',
  `username` varchar(255) DEFAULT NULL COMMENT '用户名',
  `password` varchar(255) DEFAULT NULL COMMENT '密码',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `phone` varchar(255) DEFAULT NULL COMMENT '手机号',
  `question` varchar(255) DEFAULT NULL COMMENT '密保问题',
  `answer` varchar(255) DEFAULT NULL COMMENT '密保答案',
  `role` int(255) DEFAULT NULL COMMENT '用户身份:1-普通用户.0-管理员用户',
  `headpic` varchar(255) DEFAULT NULL COMMENT '头像地址',
  `create_time` datetime DEFAULT NULL COMMENT '用户创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '用户信息更新时间',
  `createTime` date DEFAULT NULL,
  `updateTime` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_name_unique` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tb_user
-- ----------------------------
INSERT INTO `tb_user` VALUES ('1', 'admin', '427338237BD929443EC5D48E24FD2B1A', 'admin@happymmall.com', '0017759071514', '问题', '答案', '1', null, '2016-11-06 16:56:45', '2017-04-04 19:27:36', null, null);
INSERT INTO `tb_user` VALUES ('2', 'xuezipeng', 'QPF0SXOVUJUKWYSXOZ16KW', 'xuezipeng@qq.com', '17759071514', 'haha144', 'haha1454,', '0', 'http://49.232.168.42/af54ba97-0829-457b-a77e-957c825d7879.jpg', '2019-10-08 18:03:41', '2019-10-08 18:03:43', null, null);
INSERT INTO `tb_user` VALUES ('3', 'admin123', 'AZICONU9CYUFFVBP3XI1AA', '123@qq.com', '17759071514', 'admin123', 'admin123', '1', null, '2019-10-09 15:52:07', '2019-10-09 15:52:07', null, null);
INSERT INTO `tb_user` VALUES ('4', 'xuezipeng123', '9/R5ARQDWHMT2NAHL0AIQA', '1243@qq.com', '177590715143', '123', '123', '1', null, '2019-10-10 00:02:14', '2019-10-10 00:02:14', null, null);
INSERT INTO `tb_user` VALUES ('5', 'qiushi', 'PRHOBFNDCMPIF9SFY/DKYQ', '125@qq.com', '17759071513', 'haha14', 'haha1454', '1', null, '2019-10-12 08:53:59', '2019-10-12 08:53:59', null, null);
INSERT INTO `tb_user` VALUES ('6', 'xuezipeng5', 'ICY5YQXZB1UWSWCVLSNLCA', '1234@qq.com', '17759071514', '123', '123', '1', null, '2019-10-12 16:05:00', '2019-10-12 16:05:00', null, null);
INSERT INTO `tb_user` VALUES ('8', 'wzj', 'S4WLIRABLI3B918SARMFUG', '115163@qq.com', '17347456473', 'xzplovewzj', 'love', '1', '362f9677-df54-4a4e-af8a-ceb2f2c6cc43.jpg', '2019-10-12 16:12:29', '2019-10-12 16:12:29', null, null);
INSERT INTO `tb_user` VALUES ('9', '薛梓芃', 'QPF0SXOVUJUKWYSXOZ16KW', '1235@qq.com', '17759071514', 'å¦æ', '一定', '1', 'ce4fc3eb-7ed5-4187-90b1-3b4950cd9c2b.jpg', '2019-10-12 16:32:35', '2019-10-12 16:32:35', null, null);
INSERT INTO `tb_user` VALUES ('10', '孟成', 'YVGJ37UF/+0NAKEW9YXC+G', '123747@qq.com', '17759071514', '哈哈哈', '哈哈哈', '1', '90d6eca9-5578-41c9-ad84-cd07daa26c03.jpg', '2019-10-12 16:42:42', '2019-10-12 16:42:42', null, null);
INSERT INTO `tb_user` VALUES ('11', 'ceshiname', 'NX6ZVY6BM9OL99YZIRHXAA', '2531639947@qq.com', '17759071551', 'haha14', 'haha1454', '1', '', '2019-10-25 23:35:45', '2019-10-25 23:35:45', null, null);
INSERT INTO `tb_user` VALUES ('12', '1236444', 'QPF0SXOVUJUKWYSXOZ16KW', '1253@qq.com', '17755907151', 'haha144', 'haha1454', '1', null, '2019-10-25 23:39:44', '2019-10-25 23:39:44', null, null);
INSERT INTO `tb_user` VALUES ('13', '555231', 'QPF0SXOVUJUKWYSXOZ16KW', '125113@qq.com', '17757571514', 'haha14', 'haha1454', '1', 'http://49.232.168.42/taobaoHead.jpg', '2019-10-25 23:41:11', '2019-10-25 23:41:11', null, null);
INSERT INTO `tb_user` VALUES ('14', 'qiushis', 'QPF0SXOVUJUKWYSXOZ16KW', '1273@qq.com', '17759071819', 'haha14', 'haha1454', '1', 'http://49.232.168.42/taobaoHead.jpg', '2019-10-30 10:35:48', '2019-10-30 10:35:48', null, null);
INSERT INTO `tb_user` VALUES ('15', 'mengqilong', 'S3OMUIS3AF1480IWJNAISG', '12635@qq.com', '17759071514', '????', '???', '1', 'http://49.232.168.42/fefde674-3522-4ef7-9512-31d4013ec7dd.jpg', '2019-10-30 15:55:24', '2019-10-30 15:55:24', null, null);
INSERT INTO `tb_user` VALUES ('16', 'lichanglin', 'QPF0SXOVUJUKWYSXOZ16KW', '124124@qq.com', '17755571514', 'haha144..', 'haha1454', '1', 'http://49.232.168.42/taobaoHead.jpg', '2019-10-30 16:16:41', '2019-10-30 16:16:41', null, null);
INSERT INTO `tb_user` VALUES ('17', 'xuezipeng444', 'QPF0SXOVUJUKWYSXOZ16KW', '123123', '123123', '123123', '123123', '1', 'http://49.232.168.42/taobaoHead.jpg', '2019-10-30 16:17:40', '2019-10-30 16:17:40', null, null);
INSERT INTO `tb_user` VALUES ('18', 'mengcheng', 'QPF0SXOVUJUKWYSXOZ16KW', '12312356@qq.com', '17759074444', 'mibao', 'daan', '1', 'http://49.232.168.42/cc61a67f-e43b-478a-b3aa-5fd690f4b811.jpg', '2019-11-01 22:10:47', '2019-11-01 22:10:47', null, null);
INSERT INTO `tb_user` VALUES ('19', 'wuzhijiesb', 'WCSVIB0KGOSXMSD+G1+RLG', '1123423@qq.com', '17669071514', '123123', '123123', '1', 'http://49.232.168.42/taobaoHead.jpg', '2019-11-23 12:22:31', '2019-11-23 12:22:31', null, null);
INSERT INTO `tb_user` VALUES ('20', '???', 'S4WLIRABLI3B918SARMFUG', '1151636204@qq.com', '17347456473', '????', '???', '1', 'http://49.232.168.42/taobaoHead.jpg', '2019-11-23 12:25:03', '2019-11-23 12:25:03', null, null);
